﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manos_Block_Battle_Match_Server
{

    public enum GAME_MODE
    {
        NORMAL_GAME,
        RANK_GAME,
        END
    }
    public enum PROTOCOL_FRIENDSERVER_MATCH : short
    {
        Normal_Game_Request,
        One_One_Match_Request,
        One_One_One_Match_Request,
        One_One_One_One_Match_Request,
        Two_Two_Match_Request,
        Two_Two_Match_Solo_Request,
        One_One_Match_Item_Request,
        One_One_One_Match_Item_Request,
        One_One_One_One_Match_Item_Request,
        Two_Two_Match_Item_Request,
        Two_Two_Match_Solo_Item_Request,
        CREATE_ROOM_COMPLETE,
        Match_Cancel,
        END
    }

    public enum PROTOCOL_GAMESERVER_MATCH : short
    {
        CREATE_ROOM_REQUEST,
        CREATE_ROOM_COMPLETE,
        GAME_ROOM_DELETE,
        END
    }

    public enum MATCH_TYPE : short
    {
        ONE_ONE_MATCH,
        TWO_TWO_MATCH,
        ONE_ONE_ONE_MATCH,
        ONE_ONE_ONE_ONE_MATCH
    }
}
