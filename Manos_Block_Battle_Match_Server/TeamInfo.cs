﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManosNet;

namespace Manos_Block_Battle_Match_Server
{
    //매치 팀 정보
    class TeamInfo
    {
        private List<UserInfo> user_list;
        private UserToken token;
        private int friend_server_room_number;
        private MATCH_TYPE match_type;
        public TeamInfo(UserToken token)
        {
            user_list = new List<UserInfo>();
            this.token = token;
        }

        public void Set_Team_Info(Packet msg, short user_count, MATCH_TYPE match_type)
        {
            this.match_type = match_type;
            friend_server_room_number = msg.pop_int32();
            short rank_score, team_index;
            string user_id, nick_name;

            for(int i = 0; i < user_count; i++)
            {
                rank_score = msg.pop_int16();
                team_index = msg.pop_int16();
                user_id = msg.pop_string();
                nick_name = msg.pop_string();
                AddUserInfo(new UserInfo(user_id, nick_name, rank_score, team_index));
            }
        }

        public void Send_Game_Server_Info(short room_number, string adress, short port)
        {
            Packet msg = Packet.create((short)PROTOCOL_FRIENDSERVER_MATCH.CREATE_ROOM_COMPLETE);
            msg.push(friend_server_room_number);
            msg.push(room_number);
            msg.push(adress);
            msg.push(port);

            token.send(msg);
        }
        private void AddUserInfo(UserInfo userinfo)
        {
            user_list.Add(userinfo);
        }

        public short Get_Average_score()
        {
            short score = 0;
            foreach(var child in user_list)
            {
                score += child.rank_score;
            }

            return (short)(score / user_list.Count);
        }

        public List<UserInfo> Get_User_List()
        {
            return user_list;
        }

        public int Get_User_Count()
        {
            return user_list.Count;
        }

        public MATCH_TYPE Get_Match_Type()
        {
            return match_type;
        }

        public void Set_Team_Index(short team_index)
        {
            foreach(var child in user_list)
            {
                child.team_index = team_index;
            }
        }
    }
}
