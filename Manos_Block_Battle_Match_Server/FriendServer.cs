﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManosNet;

namespace Manos_Block_Battle_Match_Server
{
    class FriendServer : IPeer
    {
        UserToken token;

        public FriendServer(UserToken token)
        {
            this.token = token;
            this.token.set_peer(this);
        }

        void IPeer.on_message(byte[] buffer)
        {
            byte[] clone = new byte[1024];
            Array.Copy(buffer, clone, buffer.Length);
            Packet msg = new Packet(clone, this);
            Program.friendServerManager.Enqueue_Packet(msg);
        }

        void IPeer.on_removed()
        {
            Program.remove_friend_server(this);
            Console.WriteLine("The client disconnected.");
        }

        public void send(Packet msg)
        {
            this.token.send(msg);
        }

        void IPeer.disconnect()
        {
            this.token.Get_Socket().Disconnect(false);
        }

        void IPeer.process_user_operation(Packet msg)
        {
            //Packet msg = new Packet(buffer, this);
            PROTOCOL_FRIENDSERVER_MATCH protocol = (PROTOCOL_FRIENDSERVER_MATCH)msg.pop_protocol_id();
            Console.WriteLine("------------------------------------------------------");
            Console.WriteLine("protocol id " + protocol);
            switch (protocol)
            {
                case PROTOCOL_FRIENDSERVER_MATCH.Normal_Game_Request:
                    //룸서버 룸번호, 인원수 
                    //Program.Normal_Game_Request(token, msg.pop_int16(), msg.pop_int16());
                    Program.gameServerManager.Get_GameServer_Info(token, msg.pop_int16(), msg.pop_int16());
                    break;

                case PROTOCOL_FRIENDSERVER_MATCH.One_One_Match_Request:
                    //프렌드서버의 룸번호, 유저정보 받는다
                    TeamInfo team_info = new TeamInfo(token);
                    team_info.Set_Team_Info(msg, 1, MATCH_TYPE.ONE_ONE_MATCH);
                    Program.match(team_info);
                    //매치 큐에 해당 팀인포 전달

                    break;

                case PROTOCOL_FRIENDSERVER_MATCH.One_One_One_Match_Request:

                    break;

                case PROTOCOL_FRIENDSERVER_MATCH.One_One_One_One_Match_Request:

                    break;

                case PROTOCOL_FRIENDSERVER_MATCH.Two_Two_Match_Request:

                    break;

                case PROTOCOL_FRIENDSERVER_MATCH.Two_Two_Match_Solo_Request:

                    break;

            }
        }
    }
}
