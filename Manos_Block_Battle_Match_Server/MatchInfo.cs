﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManosNet;

namespace Manos_Block_Battle_Match_Server
{
    //매치된 유저 정보들
    class MatchInfo
    {
        private int match_index;//게임서버로 부터 받은 방 정보를 프렌드룸서버에 전달할때 찾기 위한 인덱스
        private List<TeamInfo> team_info_list;

        public MatchInfo()
        {
            team_info_list = new List<TeamInfo>();
        }

        public void AddTeamInfo(TeamInfo teaminfo)
        {
            team_info_list.Add(teaminfo);
        }

        public short Get_User_Count()
        {
            int count = 0;
            foreach(var child in team_info_list)
            {
                count += child.Get_User_Count();
            }
            return (short)count;
        }

        public void Set_Match_Index(int match_index)
        {
            this.match_index = match_index;
        }

        public void Send_Game_Server_Info(short room_number, string address, short port)
        {
            foreach(var child in team_info_list)
            {
                child.Send_Game_Server_Info(room_number, address, port);
            }
        }

        public Packet createMessage()
        {
            //매치 인덱스, 인원수, 유저 고유 id, 닉네임, 점수, 팀정보(solo, red, blue)
            Packet msg = Packet.create((short)PROTOCOL_GAMESERVER_MATCH.CREATE_ROOM_REQUEST);
            short user_count = Get_User_Count();
            msg.push(match_index);
            msg.push(user_count);
            
            foreach(var team in team_info_list)
            {
                foreach(var user in team.Get_User_List())
                {
                    msg.push(user.user_id);
                    msg.push(user.nick_name);
                    msg.push(user.rank_score);
                    msg.push(user.team_index);
                }
            }

            return msg;
        }
    }
}
