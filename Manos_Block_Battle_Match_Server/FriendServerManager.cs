﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using ManosNet;

namespace Manos_Block_Battle_Match_Server
{
    class FriendServerManager
    {
        private object operation_lock;
        private Queue<Packet> user_operations;
        private Thread logic_Thread;
        private AutoResetEvent loop_event;

        public FriendServerManager()
        {
            operation_lock = new object();
            loop_event = new AutoResetEvent(false);
            user_operations = new Queue<Packet>();



            logic_Thread = new Thread(gameLoop);
            logic_Thread.Start();
        }

        private void gameLoop()
        {
            while (true)
            {
                Packet packet = null;
                lock (operation_lock)
                {
                    if (user_operations.Count > 0)
                    {
                        packet = user_operations.Dequeue();
                    }
                }

                if (packet != null)
                {
                    //packet 처리
                    process_receive(packet);
                }

                if (user_operations.Count <= 0)
                {
                    loop_event.WaitOne();
                }
            }
        }

        public void Enqueue_Packet(Packet packet)
        {
            lock (operation_lock)
            {
                user_operations.Enqueue(packet);
                loop_event.Set();
            }
        }

        private void process_receive(Packet packet)
        {
            packet.owner.process_user_operation(packet);
        }
    }
}
