﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManosNet;
using System.Net;

namespace Manos_Block_Battle_Match_Server
{
    class GameServer : IPeer
    {
        UserToken token;
        //private int connected_user_count = 0;
        private Queue<short> room_number_queue;
        private string IP_Address;
        private int port;
        private short key_value;

        public GameServer(UserToken token)
        {
            this.token = token;
            this.token.set_peer(this);
            IP_Address = ((IPEndPoint)(token.Get_Socket().RemoteEndPoint)).Address.ToString();
            port = ((IPEndPoint)(token.Get_Socket().RemoteEndPoint)).Port;
            room_number_queue = new Queue<short>();
            for(short i = 0; i < 900; i++)
            {
                room_number_queue.Enqueue((short)(i + 100));
            }
        }

        void IPeer.on_message(byte[] buffer)
        {
            // ex)
            byte[] clone = new byte[1024];
            Array.Copy(buffer, clone, buffer.Length);
            Packet msg = new Packet(clone, this);
            Program.gameServerManager.Enqueue_Packet(msg);
        }

        void IPeer.on_removed()
        {
            Program.remove_game_server(this);
            Console.WriteLine("The client disconnected.");

        }

        public void send(Packet msg)
        {
            this.token.send(msg);
        }

        void IPeer.disconnect()
        {
            this.token.Get_Socket().Disconnect(false);
        }

        void IPeer.process_user_operation(Packet msg)
        {
            //Packet msg = new Packet(buffer, this);
            PROTOCOL_GAMESERVER_MATCH protocol = (PROTOCOL_GAMESERVER_MATCH)msg.pop_protocol_id();
            Console.WriteLine("------------------------------------------------------");
            Console.WriteLine("protocol id " + protocol);
            switch (protocol)
            {
                case PROTOCOL_GAMESERVER_MATCH.CREATE_ROOM_COMPLETE:
                    //게임서버룸번호 받고 룸번호로 어떤 프렌드 서버인지 찾아야함
                    //게임서버 ip주소, 게임서버룸번호

                    Program.gameServerManager.Send_GameServerInfo_To_FriendServer(msg, IP_Address);
                    //매치 매니저에 매치 인덱스와, roomnumber, 게임서버 ip,port 같이 전달
                    //Program.match_result(match_index, room_number, IP_Address, (short)port);
                    break;

                case PROTOCOL_GAMESERVER_MATCH.GAME_ROOM_DELETE:

                    //삭제된 게임서버 룸번호 enqueue
                    break;
            }
        }

        public string Get_IP_Address()
        {
            return IP_Address;
        }
        
        public short Dequeue_GameServer_RoomNumber()
        {
            if (room_number_queue.Count < 1)
                return -1;
            return room_number_queue.Dequeue();
        }

        public void Enqueue_GameServer_RoomNumber(short room_num)
        {
            room_number_queue.Enqueue(room_num);
        }
        public int Get_Current_Room_Count()
        {
            if (room_number_queue.Count < 0)
                return 0;

            return room_number_queue.Count;
        }

        public void Set_Key_Value(short value)
        {
            key_value = value;
        }

        public short Get_Key_Value()
        {
            return key_value;
        }
    }
}
