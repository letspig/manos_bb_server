﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manos_Block_Battle_Match_Server
{
    class MatchManager
    {
        //점수별, 게임 종류별 매치 큐를 갖는다.
        //먼저 게임 종류별(1:1, 2:2)로 나누고 그 안에서 점수별로 나눈다.
        //생성할 방 번호 만들고 게임서버로 보내주고 방생성이 완료됐다는 요청을 받으면 해당 번호로 프렌드서버에 보내줄것 
        private object operation_lock;
        private short max_grade_count = 18;
        private Match_One_One[] m_Match_One_One;
        private int mmr_score_gap = 200;
        private Dictionary<int, MatchInfo> Match_Info_Dic;
        private Queue<int> match_index_queue;
        private int Max_Match_Index_Num = 200000000;
        public MatchManager()
        {
            operation_lock = new object();
            m_Match_One_One = new Match_One_One[max_grade_count];
            for(int i = 0; i < max_grade_count; i++)
            {
                m_Match_One_One[i] = new Match_One_One();
                //m_Match_One_One[i].
            }
            Match_Info_Dic = new Dictionary<int, MatchInfo>();
            match_index_queue = new Queue<int>();
        }

        private void Match_Complete(MatchInfo match_info)
        {
            int match_index = Create_Match_Index();
            match_info.Set_Match_Index(match_index);
            Match_Info_Dic.Add(match_index, match_info);
            Program.match_complete(match_info);
        }

        public void Find_Match_Info_And_Send(int match_index, short room_number, string address, short port)
        {
            lock (Match_Info_Dic)
            {
                if (Match_Info_Dic.ContainsKey(match_index))
                {
                    Match_Info_Dic[match_index].Send_Game_Server_Info(room_number, address, port);
                    Match_Info_Dic.Remove(match_index);
                }
            }
        }

        private int Create_Match_Index()
        {
            for(int i = 0; i < Max_Match_Index_Num; i++)
            {
                if (match_index_queue.Contains(i))
                    continue;
                else
                {
                    match_index_queue.Enqueue(i);
                    return i;
                }
            }
            
            return -1;
        }
        public void Match_One_One(TeamInfo teamInfo)
        {
            //요청한 프렌드서버의 룸정보와 유저 정보를 보관한다.(상태값도 보관한다. 만약에 유저가 매치를 취소하거나 앱을 꺼버리거나 인터넷을 종료하면 해당 매치를 취소하고 다른 유저는
            //다시 매치큐에 넣어야 하기 때문이다)
            lock (operation_lock)
            {
                short rank_score = teamInfo.Get_Average_score();
                MATCH_TYPE match_type = teamInfo.Get_Match_Type();
                short match_queue_index = 0;
                //해당 점수에 맞는 큐에 넣기
                for(int i = 0; i < max_grade_count; i++)
                {
                    if (i * mmr_score_gap <= rank_score)
                        match_queue_index++;
                    else
                        break;
                }

                switch (match_type)
                {
                    case MATCH_TYPE.ONE_ONE_MATCH:
                        m_Match_One_One[match_queue_index].Match_Add(teamInfo, Match_Complete);
                        break;

                    case MATCH_TYPE.ONE_ONE_ONE_MATCH:

                        break;

                    case MATCH_TYPE.ONE_ONE_ONE_ONE_MATCH:

                        break;
                }
            }
        }
    }
}
