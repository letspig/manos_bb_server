﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManosNet;
using System.Threading;

namespace Manos_Block_Battle_Match_Server
{
    class GameServerManager
    {
        //연결된 게임서버 관리자
        //각 서버 토큰 갖고 있음
        //게임서버와 통신하면서 각 게임서버에 유저 몇명있는지 체크?아니면 방 생성 요청시 카운트 증가하고
        //게임서버가 방 삭제 했다고 알려주면 그때 카운드 감소 
        //가장 인원이 적은 서버에 방생성 요청 보내기
        private Dictionary<int, GameServer> game_Server_Dic;
        private Stack<short> GameServer_Num_Stack;
        private Dictionary<short, FriendServerInfo> Friend_Server_Info_Dic;

        private object operation_lock;
        private Queue<Packet> user_operations;
        private Thread logic_Thread;
        private AutoResetEvent loop_event;

        public GameServerManager()
        {
            game_Server_Dic = new Dictionary<int, GameServer>();
            Friend_Server_Info_Dic = new Dictionary<short, FriendServerInfo>();
            operation_lock = new object();
            loop_event = new AutoResetEvent(false);
            user_operations = new Queue<Packet>();
            GameServer_Num_Stack = new Stack<short>();
            for(short i = 0; i < 1000; i++)
            {
                GameServer_Num_Stack.Push(i);
            }
            logic_Thread = new Thread(gameLoop);
            logic_Thread.Start();

        }

        private void gameLoop()
        {
            while (true)
            {
                Packet packet = null;
                lock (operation_lock)
                {
                    if (user_operations.Count > 0)
                    {
                        packet = user_operations.Dequeue();
                    }
                }

                if (packet != null)
                {
                    //packet 처리
                    process_receive(packet);
                }

                if (user_operations.Count <= 0)
                {
                    loop_event.WaitOne();
                }
            }
        }

        public void Enqueue_Packet(Packet packet)
        {
            lock (operation_lock)
            {
                user_operations.Enqueue(packet);
                loop_event.Set();
            }
        }

        private void process_receive(Packet packet)
        {
            packet.owner.process_user_operation(packet);
        }

        public void AddGameServer(GameServer gameServer)
        {
            lock (game_Server_Dic)
            {
                if (GameServer_Num_Stack.Count < 1)
                    return;

                short num = GameServer_Num_Stack.Pop();
                gameServer.Set_Key_Value(num);
                game_Server_Dic.Add(num, gameServer);
            }
            
        }

        public void Send_GameServerInfo_To_FriendServer(Packet msg, string address)
        {
            
            lock (Friend_Server_Info_Dic)
            {
                if (Friend_Server_Info_Dic.Count < 1)
                    return;
                short request_num = msg.pop_int16();
                short gameroom_num = msg.pop_int16();
                if (Friend_Server_Info_Dic.ContainsKey(request_num))
                {
                    Friend_Server_Info_Dic[request_num].Send(address, gameroom_num);
                }
            }

        }
        
        public void Get_GameServer_Info(UserToken token, short roomserver_roomnum, short user_count)
        {
            //룸이 가장 적은 서버의 주소와 룸넘버 받기
            

            lock (game_Server_Dic)
            {
                if (game_Server_Dic.Count < 1)
                {
                    return;
                }
                int room_count = game_Server_Dic[0].Get_Current_Room_Count();
                if (room_count < 1)
                    return;
                int next_room_count = 0; ;
                GameServer gameserver = game_Server_Dic[0];
                
                foreach (var child in game_Server_Dic)
                {
                    next_room_count = child.Value.Get_Current_Room_Count();
                    if (next_room_count < 1)
                        continue;
                    if (room_count < next_room_count)
                    {
                        room_count = next_room_count;
                        gameserver = child.Value;
                    }
                }
                if (room_count < 1)
                    return;


                //게임 서버에 방 생성 요청하기
                //list에 요청자 정보 넣고 요청할것. 요청자 정보(요청번호), 게임모드, 인원수
                
                short request_num = gameserver.Dequeue_GameServer_RoomNumber();
                if (request_num == -1)
                    return;

                Add_FriendServerInfo(request_num, new FriendServerInfo(token, roomserver_roomnum));

                Packet game_server_msg = Packet.create((short)PROTOCOL_GAMESERVER_MATCH.CREATE_ROOM_REQUEST);
                game_server_msg.push(request_num);
                game_server_msg.push((short)GAME_MODE.NORMAL_GAME);
                game_server_msg.push(user_count);
                gameserver.send(game_server_msg);

                //게임서버에 방생성 요청 보내면서 룸서버 정보 따로 빼놓기.
                
            }

        }

        private void Add_FriendServerInfo(short key, FriendServerInfo info)
        {
            lock (Friend_Server_Info_Dic) 
            {
                if (Friend_Server_Info_Dic.ContainsKey(key))
                {
                    Friend_Server_Info_Dic[key] = info;
                }
                else
                {
                    Friend_Server_Info_Dic.Add(key, info);
                }
            }
        }

        public void Remove_GameServer(GameServer server)
        {
            lock (game_Server_Dic)
            {
                if (game_Server_Dic.ContainsKey(server.Get_Key_Value()))
                {
                    game_Server_Dic.Remove(server.Get_Key_Value());
                }
            }
        }

        
    }
}
