﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManosNet;

namespace Manos_Block_Battle_Match_Server
{
    
    class Program
    {
        private static List<FriendServer> friend_server_list;
        public static GameServerManager gameServerManager = new GameServerManager();
        private static MatchManager matchManager = new MatchManager();
        public static FriendServerManager friendServerManager = new FriendServerManager();
        static void Main(string[] args)
        {
            friend_server_list = new List<FriendServer>();

            PacketBufferManager.initialize(8000);

            NetworkService friend_networkService = new NetworkService();
        
            friend_networkService.session_created_callback += on_session_created_friend_server;
            friend_networkService.initialize();
            friend_networkService.listen("0.0.0.0", 7979, 100);

            NetworkService game_server_networkService = new NetworkService();

            game_server_networkService.session_created_callback += on_session_created_game_server;
            game_server_networkService.initialize();
            game_server_networkService.listen("0.0.0.0", 8989, 100);

            Console.WriteLine("Server Start");

            while (true)
            {
                System.Threading.Thread.Sleep(1000);
            }
        }

        static public void Normal_Game_Request(UserToken token, short roomserver_roomnumber, short user_count)
        {
            gameServerManager.Get_GameServer_Info(token, roomserver_roomnumber, user_count);
        }


        static public void match(TeamInfo teamInfo)
        {
            matchManager.Match_One_One(teamInfo);
        }

        static public void match_result(int match_index, short room_number, string address, short port)
        {
            matchManager.Find_Match_Info_And_Send(match_index, room_number, address, port);
        }

        static public void match_complete(MatchInfo matchInfo)
        {
            //gameServerManager.Request_Room_Create_To_GameServer(matchInfo);
        }

        static void on_session_created_friend_server(UserToken token)
        {
            lock (friend_server_list)
            {
                friend_server_list.Add(new FriendServer(token));
            }
            Console.WriteLine("friend_server session created.");
        }

        static public void remove_friend_server(FriendServer server)
        {
            lock (friend_server_list)
            {
                if (friend_server_list.Contains(server))
                {
                    friend_server_list.Remove(server);
                    Console.WriteLine("friend_server removed.");
                }
                    
            }
        }

        static void on_session_created_game_server(UserToken token)
        {
            gameServerManager.AddGameServer(new GameServer(token));
            Console.WriteLine("game_server session created.");
        }

        static public void remove_game_server(GameServer server)
        {
            gameServerManager.Remove_GameServer(server);
        }
    }

    
}
