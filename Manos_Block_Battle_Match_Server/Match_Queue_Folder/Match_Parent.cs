﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manos_Block_Battle_Match_Server
{
    class Match_Parent
    {
        //매치 큐<team index>
        //매칭 인원수
        //매칭 되면 게임 서버로 방생성 요청하기
        //그러면 게임서버 관리자도 필요하다. 각 게임서버에 연결된 유저수를 파악해야 하기 때문
        //사람이 적은 서버로 방생성 요청 해줘야 한다.
        protected short match_user_count;
        protected Queue<TeamInfo> match_queue;

        public delegate void Match_Complete_Callback(MatchInfo match_info);
        public Match_Parent()
        {

        }

        virtual public void Match_Add(TeamInfo teamInfo, Match_Complete_Callback callback)
        {
            if (match_queue.Contains(teamInfo))
                return;

            match_queue.Enqueue(teamInfo);
            //team index 정해주자

            if (match_queue.Count == match_user_count)
            {
                //매칭이 됐으므로 게임서버관리자한테 방생성 요청하라고 알려준다
                //매치 인포 만들어서 보낸다
                //큐는 비운다
                MatchInfo match_info = new MatchInfo();

                for(int i = 0; i < match_user_count; i++)
                {
                    match_info.AddTeamInfo(match_queue.Dequeue());
                }
                Program.match_complete(match_info);
                match_queue.Clear();
            }
        }
    }
}
