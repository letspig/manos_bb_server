﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manos_Block_Battle_Match_Server
{
    class Match_One_One : Match_Parent
    {
        
        //public Match_Complete_Callback match_complete;


        public Match_One_One()
        {
            match_user_count = 2;
            match_queue = new Queue<TeamInfo>(2);
        }

        override public void Match_Add(TeamInfo teamInfo, Match_Complete_Callback callback)
        {
            if (match_queue.Contains(teamInfo))
                return;

            //team index 정해주자 1:1매치 이므로 0
            teamInfo.Set_Team_Index(0);
            match_queue.Enqueue(teamInfo);


            if (match_queue.Count == match_user_count)
            {
                //매칭이 됐으므로 게임서버관리자한테 방생성 요청하라고 알려준다
                //매치 인포 만들어서 보낸다
                //큐는 비운다
                //매치 인포 따로 갖고 있다가 방생성 완료 되면 매치 인포 찾아서 알려준다.
                MatchInfo match_info = new MatchInfo();

                for (int i = 0; i < match_user_count; i++)
                {
                    match_info.AddTeamInfo(match_queue.Dequeue());
                }
                callback(match_info);
                
                match_queue.Clear();
            }
        }
    }
}
