﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManosNet;

namespace Manos_Block_Battle_Match_Server
{
    class FriendServerInfo
    {
        private UserToken token;
        private short friend_server_roomnumber;

        public FriendServerInfo(UserToken token, short room_number)
        {
            this.token = token;
            this.friend_server_roomnumber = room_number;
        }

        public void Send(string address, short room_num)
        {
            Packet msg = Packet.create((short)PROTOCOL_FRIENDSERVER_MATCH.CREATE_ROOM_COMPLETE);
            msg.push(friend_server_roomnumber);
            msg.push(address);
            msg.push(room_num);

            token.send(msg);
            
        }
    }
}
