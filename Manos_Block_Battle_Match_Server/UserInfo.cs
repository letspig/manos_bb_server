﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManosNet;

namespace Manos_Block_Battle_Match_Server
{
    //매치 요청한 유저 정보
    
    class UserInfo
    {
        public string user_id;
        public string nick_name;
        public short rank_score;
        public short team_index;

        public UserInfo(string user_id, string nick_name, short rank_score, short team_index = 0)
        {
            this.user_id = user_id;
            this.nick_name = nick_name;
            this.rank_score = rank_score;
            this.team_index = team_index;
        }

        public void Set_Team_Index(short team_index)
        {
            this.team_index = team_index;
        }
    }
}
