﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using ManosNet;

namespace Manos_Block_Battle_Friend_Room_Server
{
    class GameUser : IPeer
    {
        UserToken token;
        FriendRoom friend_room;
        public UserInfo user_info;
        public short servernum;
        private DynamoDBClient db_client;
        private FieldLogicProcess fieldlogicprocess;
        public GameUser()
        {
            db_client = new DynamoDBClient();
            user_info = new UserInfo();
            //임시로 유저인포 셋팅(db 생기면 안함)
            // 닉넴, 티어, 하트수, 별포수, 티어점수, 승급전 승패, 1,2,3등 전적수, mmr
            user_info.user_id = "asdf1234";
            user_info.team_index = 0;
            user_info.mmr = 5;
            user_info.nick_name = "닉네임최대아홉글자";
            user_info.tier = 5;
            user_info.heart_count = 5;
            user_info.star_point = 8031234;
            user_info.tier_score = 25;
            user_info.promotion = "101";
            user_info.first_win = 15;
            user_info.second_win = 45;
            user_info.third_win = 100;
            user_info.total_play_count = 193;
            user_info.straight_win_count = 8;
            user_info.losing_streak_count = 4;
            user_info.last_connected_time = "200405003640";
            user_info.heart_free_item_time = "200505003640";
        }

        public void Reset()
        {
            friend_room = null;
            user_info.client_state = false;
        }

        public GameUser(UserToken token)
        {
            this.token = token;
            this.token.set_peer(this);
            
        }

        public UserInfo Get_User_Info()
        {
            return user_info;
        }

        public void Set_FieldLogic(FieldLogicProcess logic)
        {
            fieldlogicprocess = logic;
        }

        public FieldLogicProcess Get_FieldLogic()
        {
            return fieldlogicprocess;
        }

        public void Set_UserToken(UserToken token)
        {
            this.token = token;
            this.token.set_peer(this);
        }

        void IPeer.on_message(byte[] buffer)
        {
            byte[] clone = new byte[1024];
            Array.Copy(buffer, clone, buffer.Length);
            Packet msg = new Packet(clone, this);
            Program.friendServer.Enqueue_Packet(msg);
        }

        void IPeer.on_removed()
        {
            Console.WriteLine("The client disconnected.");
            if(friend_room != null)
            {
                friend_room.LeaveRoom(this);
            }
            Program.remove_user(this);
        }

        public void send(Packet msg)
        {
            this.token.send(msg);
        }

        void IPeer.disconnect()
        {
            this.token.Get_Socket().Disconnect(false);
        }

        void IPeer.process_user_operation(Packet msg)
        {
            PROTOCOL_CLIENT_FRIENDSERVER protocol = (PROTOCOL_CLIENT_FRIENDSERVER)msg.pop_protocol_id();
            short room_number;
            switch (protocol)
            {
                case PROTOCOL_CLIENT_FRIENDSERVER.BLOCK:
                    if (friend_room == null)
                        return;
                    friend_room.Block_Put(this, msg, user_info.field_index);
                    break;

                case PROTOCOL_CLIENT_FRIENDSERVER.CREATE_ROOM:
                    if (friend_room != null)
                        return;
                    Program.friendServer.Create_Room(this, msg);
                    break;

                case PROTOCOL_CLIENT_FRIENDSERVER.ACTIVATE_ROOM:
                    //클라이언트가 준비가 되어있으니 서버로부터 데이터를 받아도 된다
                    //룸에 다른 유저가 입장 가능하도록 활성화
                    if (friend_room == null)
                        return;
                    user_info.client_state = true;
                    friend_room.Set_Room_State(1);
                    //Packet state_msg = Packet.create((short)PROTOCOL_CLIENT_FRIENDSERVER.ROOM_CREATE_COMPLETE);
                    //send(state_msg);
                    break;

                case PROTOCOL_CLIENT_FRIENDSERVER.ENTER_ROOM:

                    //유저 정보 꺼내서 userinfo 셋 하고 
                    //룸 입장
                    room_number = msg.pop_int16();
                    Program.friendServer.EnterGameRoom(this, room_number);
                    break;

                case PROTOCOL_CLIENT_FRIENDSERVER.USER_STATE:
                    if (friend_room == null)
                        return;
                    friend_room.User_State_Change(this, msg.pop_int16());
                    break;

                case PROTOCOL_CLIENT_FRIENDSERVER.NORMALGAME_START_REQUEST:
                    if (friend_room == null)
                        return;
                    friend_room.NormalGame_Start_Request(this);
                    break;

                case PROTOCOL_CLIENT_FRIENDSERVER.TEAM_CHANGE:
                    if (friend_room == null)
                        return;
                    friend_room.UserTeamChange(this, msg.pop_int16());
                    break;

                case PROTOCOL_CLIENT_FRIENDSERVER.EXIST_USER_INFO:
                    //현재 방에 있는 유저 정보 받아오기 요청
                    if (friend_room == null)
                        return;
                    //user_info.client_state = true;
                    friend_room.Exist_User_Info_Get(this);
                    break;
                    
                case PROTOCOL_CLIENT_FRIENDSERVER.LEAVE_ROOM:
                    //leave room
                    if (friend_room == null)
                        return;
                    friend_room.LeaveRoom(this);
                    friend_room = null;
                    user_info.client_state = false;
                    break;

                case PROTOCOL_CLIENT_FRIENDSERVER.FIND_ROOM_SERVER_NUM:
                    //서버번호로 해당 서버 주소 가져오기
                    break;

                case PROTOCOL_CLIENT_FRIENDSERVER.USER_BAN:
                    if (friend_room == null)
                        return;
                    friend_room.UserBan(this, msg.pop_int16());
                    break;


                case PROTOCOL_CLIENT_FRIENDSERVER.DB_REQUEST:
                    user_info.user_id = msg.pop_string();
                    user_info.nick_name = msg.pop_string();
                    db_client.Load_Data(user_info.user_id, user_info.nick_name).ContinueWith(task =>
                    {
                        if (task.Result)
                        {
                            user_info = db_client.Get_UserInfo();
                            Send_DB_Data();
                            //Console.WriteLine(user_info.last_connected_time);
                        }
                    });
                    //if(db_client.Login(user_info.user_id, user_info.nick_name))
                    //{
                    //    user_info = db_client.Get_UserInfo();
                    //    Send_DB_Data();
                    //    Console.WriteLine(user_info.last_connected_time);
                    //}
                    
                    //db서버에 유저 고유 아이디 보내서 아이디 받아오기
                    //서버번호, 닉넴, 티어, 하트수, 별포수, 티어점수, 승급전 승패, 1,2,3등 전적수
                    //Send_DB_Data();
                    break;
            }
        }


        private void Send_DB_Data()
        {
            Packet db_request = Packet.create((short)PROTOCOL_CLIENT_FRIENDSERVER.DB_REQUEST);
            db_request.push(servernum);//서버 번호
            db_request.push(user_info.nick_name);
            db_request.push(user_info.tier);
            db_request.push(user_info.heart_count);
            db_request.push(user_info.star_point);
            db_request.push(user_info.tier_score);
            db_request.push(user_info.promotion);
            db_request.push(user_info.first_win);
            db_request.push(user_info.second_win);
            db_request.push(user_info.third_win);
            db_request.push(user_info.total_play_count);
            db_request.push(user_info.straight_win_count);
            db_request.push(user_info.losing_streak_count);
            db_request.push(user_info.last_connected_time);
            db_request.push(user_info.heart_free_item_time);
            send(db_request);
        }

        public void Set_Friend_Room(FriendRoom room)
        {
            friend_room = room;
        }
    }
}
