﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using ManosNet;

namespace Manos_Block_Battle_Friend_Room_Server
{
    class FriendServer
    {
        private object operation_lock;
        private Queue<Packet> user_operations;
        private Thread logic_Thread;
        private AutoResetEvent loop_event;

        private Dictionary<short, FriendRoom> friend_room_dic;
        private RoomPool room_pool;

        private Stack<short> room_number_queue;//100~999번까지 있음

        private Dictionary<short, Server_Info> other_server_info_dic;

        private short max_room_count = 900;

        private short my_server_num = 1000;

        public FriendServer()
        {
            operation_lock = new object();
            loop_event = new AutoResetEvent(false);
            user_operations = new Queue<Packet>();
            room_number_queue = new Stack<short>();

            friend_room_dic = new Dictionary<short, FriendRoom>();
            room_pool = new RoomPool(max_room_count);
            for(short i = (short)(max_room_count - 1); i >= 0; i--)
            {
                room_pool.Push(new FriendRoom());
                room_number_queue.Push((short)(i + 100));
            }

            other_server_info_dic = new Dictionary<short, Server_Info>();

            logic_Thread = new Thread(gameLoop);
            logic_Thread.Start();
        }

        public void Find_Server_Address(GameUser user, short server_num)
        {
            lock (other_server_info_dic)
            {
                if (other_server_info_dic.ContainsKey(server_num))
                {
                    Packet server_address_msg = Packet.create((short)PROTOCOL_CLIENT_FRIENDSERVER.FIND_ROOM_SERVER_NUM);
                    server_address_msg.push(other_server_info_dic[server_num].address);
                    user.send(server_address_msg);
                }
                else
                {
                    //서버없다고 방없다고 알려주기
                }
            }
        }

        public void Add_Other_Server_Info(Packet msg)
        {
            short server_num = msg.pop_int16();
            Server_Info server_info = new Server_Info(msg.pop_string());
            lock (other_server_info_dic)
            {
                if (other_server_info_dic.ContainsKey(server_num))
                {
                    other_server_info_dic[server_num] = server_info;
                }
                else
                {
                    other_server_info_dic.Add(server_num, server_info);
                }
            }
            

        }

        public void Add_All_Server_Info_List(Packet msg)
        {
            short server_list_count = msg.pop_int16();
            lock (other_server_info_dic)
            {
                for (int i = 0; i < server_list_count; i++)
                {
                    short server_num = msg.pop_int16();
                    Server_Info server_info = new Server_Info(msg.pop_string());
                    other_server_info_dic.Add(server_num, server_info);
                }
            }
            
        }

        public void Set_My_Server_Num(short num)
        {
            my_server_num = num;
        }

        public short Get_Server_Num()
        {
            return my_server_num;
        }

        public void Find_Room_And_Send(Packet msg)
        {
            lock (friend_room_dic)
            {
                friend_room_dic[msg.pop_int16()].Send_Game_Server_Info(msg);
            }
        }

        public void Create_Room(GameUser user, Packet msg)
        {
            //룸 생성 요청. 더이상 생성 할 수 없는 방이 없으면 다른 로비 서버로 입장 해서 방 생성 유도 하기.
            lock (friend_room_dic)
            {
                if(room_number_queue.Count < 1)
                {
                    //생성 할 수 있는 방이 없다고 클라에 알려주기
                    Packet create_fail_msg = Packet.create((short)PROTOCOL_CLIENT_FRIENDSERVER.CREATE_ROOM_FAIL);
                    user.send(create_fail_msg);
                    return;
                }

                FriendRoom room = room_pool.Pop();
                short room_number = room_number_queue.Pop();
                room.Created_Room(user, room_number, (GAME_MODE)msg.pop_int16());
                friend_room_dic.Add(room_number, room);
                user.Set_Friend_Room(room);

                Packet create_room_msg = Packet.create((short)PROTOCOL_CLIENT_FRIENDSERVER.CREATE_ROOM);
                create_room_msg.push(room_number);
                create_room_msg.push(user.Get_User_Info().field_index);
                user.send(create_room_msg);

            }
        }

        public void EnterGameRoom(GameUser user, short room_number)
        {
            lock (friend_room_dic)
            {
                if (friend_room_dic.ContainsKey(room_number))
                {
                    FriendRoom existRoom = friend_room_dic[room_number];
                    if(existRoom.Get_Room_State() == 1)
                    {
                        existRoom.EnterUser(user);
                        user.Set_Friend_Room(existRoom);
                    }
                    
                }
                else
                {
                    //게임이 이미 종료 된 방일 가능성 높다.
                    //유저에게 방에 입장 할 수 없다고 알려주자.
                    Packet enter_fail_msg = Packet.create((short)PROTOCOL_CLIENT_FRIENDSERVER.ENTER_FAIL);
                    user.send(enter_fail_msg);
                }
            }
        }

        public void Remove_Room(FriendRoom room)
        {
            lock (friend_room_dic)
            {
                short room_number = room.room_number;
                if (friend_room_dic.ContainsKey(room_number))
                {
                    friend_room_dic.Remove(room_number);
                    room_pool.Push(room);
                    room_number_queue.Push(room_number);
                }
            }
        }

        private void gameLoop()
        {
            while (true)
            {
                Packet packet = null;
                lock (operation_lock)
                {
                    if (user_operations.Count > 0)
                    {
                        packet = user_operations.Dequeue();
                    }
                }

                if (packet != null)
                {
                    //packet 처리
                    process_receive(packet);
                }

                if (user_operations.Count <= 0)
                {
                    loop_event.WaitOne();
                }
            }
        }

        public void Enqueue_Packet(Packet packet)
        {
            lock (operation_lock)
            {
                user_operations.Enqueue(packet);
                loop_event.Set();
            }
        }

        private void process_receive(Packet packet)
        {
            packet.owner.process_user_operation(packet);
        }
    }
}
