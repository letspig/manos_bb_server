﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManosNet;

namespace Manos_Block_Battle_Friend_Room_Server
{
    class Program
    {
        static List<GameUser> userlist;
        public static GameUserPool user_pool;
        public static FriendServer friendServer = new FriendServer();
        private static int user_max_count = 2000;
        //private static MatchServerManager matchserverManager = new MatchServerManager();
        //private static RoomService room_service = new RoomService();
        static void Main(string[] args)
        {

            NetworkService networkService = new NetworkService();
            PacketBufferManager.initialize(8000);
            userlist = new List<GameUser>();
            user_pool = new GameUserPool(user_max_count);
            short servernum = friendServer.Get_Server_Num(); // 룸매니저 서버에 접속후에 정하기
            for (int i = 0; i < user_max_count; i++)
            {
                GameUser gameUser = new GameUser();
                gameUser.servernum = servernum;
                user_pool.Push(gameUser);
            }

            networkService.session_created_callback += on_session_created;
            networkService.initialize();
            networkService.listen("0.0.0.0", 7979, 100);

            //matchserverManager.Init();

            Console.WriteLine("Server Start");

            

            //room_service.RoomManager_Server_Connect("123.343.342.333", 7777);

            //Connector connector = new Connector(networkService);
            //connector.connected_callback += connected_gameserver;
            //IPEndPoint iPEndPoint = new IPEndPoint(IPAddress.Parse("211.250.58.163"), 8989);
            //connector.connect(iPEndPoint);

            //matchserverManager.Init();

            while (true)
            {
                System.Threading.Thread.Sleep(1000);
            }

        }

        public static void Match_Request(PROTOCOL_FRIENDSERVER_MATCH match_type, Packet msg)
        {
            //matchserverManager.Match_Request(match_type, msg);
        }

        static void on_session_created(UserToken token)
        {
            Console.WriteLine("session created.");
            lock (userlist)
            {
                GameUser user = user_pool.Pop();
                user.Set_UserToken(token);
                userlist.Add(user);
            }

        }

        public static void remove_user(GameUser user)
        {
            lock (userlist)
            {
                user.Reset();
                user_pool.Push(user);
                userlist.Remove(user);
            }
        }

        public static void connected_gameserver(UserToken userToken)
        {

        }
    }
}
