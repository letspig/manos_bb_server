﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manos_Block_Battle_Friend_Room_Server
{
    public enum PROTOCOL_FRIENDSERVER_ROOMMANAGERSERVER : short
    {
        SERVER_NUM,
        FIND_SERVER,
        ADD_SERVER_INFO,
        SERVER_LIST_INFO,
        SERVER_ADDRESS,
        ROOM_DELETE,
        END
    }

    public enum PROTOCOL_FRIENDSERVER_MATCH : short
    {
        Normal_Game_Request,
        One_One_Match_Request,
        One_One_One_Match_Request,
        One_One_One_One_Match_Request,
        Two_Two_Match_Request,
        Two_Two_Match_Solo_Request,
        One_One_Match_Item_Request,
        One_One_One_Match_Item_Request,
        One_One_One_One_Match_Item_Request,
        Two_Two_Match_Item_Request,
        Two_Two_Match_Solo_Item_Request,
        CREATE_ROOM_COMPLETE,
        Match_Cancel,
        END
    }

    public enum PROTOCOL_CLIENT_FRIENDSERVER : short
    {
        DB_REQUEST,
        CREATE_ROOM,
        CREATE_MATCH_ROOM,
        ENTER_ROOM,
        ENTER_FAIL,
        ROOM_USER_NUM_CHANGE,
        ROOM_MASTER_CHANGE,
        GAME_MODE_CHANGE,
        USER_STATE,
        NORMALGAME_START_REQUEST,
        GAME_START,
        USER_BAN,
        TEAM_CHANGE,
        LEAVE_ROOM,
        GAME_SERVER_INFO,
        JOIN_USER_INFO,
        EXIST_USER_INFO,
        FIND_ROOM_SERVER_NUM,
        GAME_RESULT,
        CREATE_ROOM_FAIL,//방생성 실패 했으니 다른 서버에서 방 만들어라
        ROOM_CREATE_COMPLETE,
        ACTIVATE_ROOM,
        BLOCK,
        MISSION_TYPE,
        BLOCK_SEED,
        COMBO_COUNT,
        LINE_CLEAR_COUNT,
        ATTACK_LINE_COUNT,
        TIME_ATTACK,
        USE_ITEM,
        End
    }

    public enum GAME_MODE : short
    {
        ONE_ONE_NOITEM_RANK,
        ONE_ONE_ONE_NOITEM_RANK,
        ONE_ONE_ONE_ONE_NOITEM_RANK,
        TWO_TWO_NOITEM_RANK,
        TWO_TWO_NOITEM_SOLO_RANK,
        ONE_ONE_ITEM_RANK,
        ONE_ONE_ONE_ITEM_RANK,
        ONE_ONE_ONE_ONE_ITEM_RANK,
        TWO_TWO_ITEM_RANK,
        TWO_TWO_ITEM_SOLO_RANK,
        NOITEM_NORMAL,
        ITEM_NORMAL,
        END
    }
}
