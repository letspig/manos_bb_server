﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.DynamoDBv2.Model;

namespace Manos_Block_Battle_Friend_Room_Server
{
    class DynamoDBClient
    {
        private string table_name = "UserInfoTable";
        private AmazonDynamoDBClient db_client;
        private UserInfo temp_user_info;

        private Table userinfo_table;

        public async Task<bool> Load_Data(string user_id, string nickname)
        {
            var read_response = await userinfo_table.GetItemAsync(user_id);

            if(read_response != null && read_response.Count > 0)
            {
                Set_User_Info(read_response);
                return true;
            }
            else
            {
                //신규 가입하기
                PutItemOperationConfig config = new PutItemOperationConfig()
                {
                    ReturnValues = ReturnValues.AllOldAttributes
                };

                var newWrite = new Document();
                newWrite["ID"] = user_id;
                newWrite["Nickname"] = nickname;
                newWrite["Tier"] = 3;
                newWrite["mmr"] = 1000;
                newWrite["Heart"] = 5;
                newWrite["Star"] = 1000;
                newWrite["TierScore"] = 0;
                newWrite["Promotion"] = "-1";
                newWrite["TotalPlay"] = 0;
                newWrite["FirstWin"] = 0;
                newWrite["SecondWin"] = 0;
                newWrite["ThirdWin"] = 0;
                newWrite["StraightWin"] = 0;
                newWrite["LosingStreak"] = 0;
                newWrite["LastConnectTime"] = DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss");
                newWrite["HeartFreeItemTime"] = "0";
                await userinfo_table.PutItemAsync(newWrite, config);
                var read_response2 = await userinfo_table.GetItemAsync(user_id);
                if (read_response2 != null && read_response2.Count > 0)
                {
                    Set_User_Info(read_response2);
                    return true;
                }

            }
            return false;
        }

        public void Login(string user_id, string nickname)
        {
            //유저 데이터 없으면 생성하고 
            //있으면 바로 가져오고 
            

            var login_task = Get_All_DataAsync(user_id).ContinueWith(task =>
            {
                
                if (task.Result != null)
                {
                    Set_User_Info(task.Result);
                    Console.WriteLine("login");
                    //return true;
                }
                else
                {
                    var newuser_task = Create_New_User(user_id, nickname).ContinueWith(task2 =>
                    {
                        return task2.Result;
                    });
                }
                return task.Result;
            });

            

            
            
            //신규유저
            //var newuser_task = Create_New_User(user_id, nickname).ContinueWith(task =>
            //{
            //    return task.Result;
            //});

            //if(newuser_task.Result.Count < 1)
            //{
            //    return false;
            //}
            //else
            //{
            //    //가입 성공
            //    Console.WriteLine("new user!");
            //    Init_New_User_Info(user_id, nickname);
            //    return true;
            //}

            

        }
        
        private void Set_User_Info(Document doc)
        {

            temp_user_info.user_id = doc["ID"];
            temp_user_info.nick_name = doc["Nickname"];
            temp_user_info.tier = doc["Tier"].AsShort();
            temp_user_info.mmr = doc["mmr"].AsShort();
            temp_user_info.heart_count = doc["Heart"].AsShort();
            temp_user_info.star_point = doc["Star"].AsInt();
            temp_user_info.tier_score = doc["TierScore"].AsShort();
            temp_user_info.promotion = doc["Promotion"];
            temp_user_info.total_play_count = doc["TotalPlay"].AsInt();
            temp_user_info.first_win = doc["FirstWin"].AsInt();
            temp_user_info.second_win = doc["SecondWin"].AsInt();
            temp_user_info.third_win = doc["ThirdWin"].AsInt();
            temp_user_info.straight_win_count = doc["StraightWin"].AsShort();
            temp_user_info.losing_streak_count = doc["LosingStreak"].AsShort();
            temp_user_info.last_connected_time = doc["LastConnectTime"];
            temp_user_info.heart_free_item_time = doc["HeartFreeItemTime"];
        }

        private void Set_User_Info(Dictionary<string, AttributeValue> user_info_dic)
        {

            foreach(KeyValuePair<string, AttributeValue> kvp in user_info_dic)
            {
                AttributeValue value = kvp.Value;
                temp_user_info.user_id = value.S;
                temp_user_info.nick_name = value.S;
                temp_user_info.tier = short.Parse(value.N);
                temp_user_info.mmr = short.Parse(value.N);
                temp_user_info.heart_count = short.Parse(value.N);
                temp_user_info.star_point = short.Parse(value.N);
                temp_user_info.tier_score = short.Parse(value.N);
                temp_user_info.promotion = value.S;
                temp_user_info.total_play_count = short.Parse(value.N);
                temp_user_info.first_win = short.Parse(value.N);
                temp_user_info.second_win = short.Parse(value.N);
                temp_user_info.third_win = short.Parse(value.N);
                temp_user_info.straight_win_count = short.Parse(value.N);
                temp_user_info.losing_streak_count = short.Parse(value.N);
                temp_user_info.last_connected_time = value.S;
                temp_user_info.heart_free_item_time = value.S;
            }
        }

        public void Get_Server_Time()
        {
            DateTime current_date_time;
            DateTime.TryParse(DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"), out current_date_time);

        }

        public UserInfo Get_UserInfo()
        {
            return temp_user_info;
        }

        //login 할 때 모든 데이터 가져오기 
        public async Task<Document> Get_All_DataAsync(string user_id)
        {

            //GetItemOperationConfig config = new GetItemOperationConfig
            //{
            //    AttributesToGet = new List<string> { "ID", "Nickname", "Tier", "mmr", "Heart", "Star", "TierScore", "Promotion", "TotalPlay",
            //        "FirstWin", "SecondWin", "ThirdWin", "StraightWin", "LosingStreak", "LastConnectTime", "HeartFreeItemTime" },
            //    ConsistentRead = true
            //};


            try
            {
                //var request = new GetItemRequest
                //{
                //    TableName = table_name,
                //    Key = new Dictionary<string, AttributeValue>() { { "ID", new AttributeValue { S = user_id } } }

                //};


                var response = await userinfo_table.GetItemAsync(user_id);

                return response;

                //var response = await userinfo_table.GetItemAsync(user_id);
                //var response = await db_client.GetItemAsync(request);

                //if (response == null)
                //    return false;
                //else
                //{
                //    //Console.WriteLine("exist");
                //    Set_User_Info(response);
                //    return true;
                //}

            }
            catch (InternalServerErrorException e)
            {
                Console.WriteLine(e);
            }
            catch (ProvisionedThroughputExceededException e)
            {
                Console.WriteLine(e);
            }
            catch (RequestLimitExceededException e)
            {
                Console.WriteLine(e);
            }
            catch (ResourceNotFoundException e)
            {
                Console.WriteLine(e);
            }
            return null;
        }

        private void User_Exist_Check()
        {

        }

        private async Task<Document> Create_New_User(string user_id, string nickname)
        {
            //var batchWrite = userinfo_table.CreateBatchWrite();
            var newWrite = new Document();
            newWrite["ID"] = user_id;
            newWrite["Nickname"] = nickname;
            newWrite["Tier"] = 3;
            newWrite["mmr"] = 1000;
            newWrite["Heart"] = 5;
            newWrite["Star"] = 1000;
            newWrite["TierScore"] = 0;
            newWrite["Promotion"] = "0";
            newWrite["TotalPlay"] = 0;
            newWrite["FirstWin"] = 0;
            newWrite["SecondWin"] = 0;
            newWrite["ThirdWin"] = 0;
            newWrite["StraightWin"] = 0;
            newWrite["LosingStreak"] = 0;
            newWrite["LastConnectTime"] = DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss");
            newWrite["HeartFreeItemTime"] = "0";



            //var request = new PutItemRequest
            //{
            //    TableName = table_name,
            //    Item = new Dictionary<string, AttributeValue>()
            //    {
            //        {"ID", new AttributeValue{ S = user_id} },
            //        {"Nickname", new AttributeValue{ S = nickname} },
            //        {"Tier", new AttributeValue{ N = "3"} },
            //        {"mmr", new AttributeValue{N = "1000" } },
            //        {"Heart", new AttributeValue{N = "5" } },
            //        {"Star", new AttributeValue{N = "500" } },
            //        {"Tier_Score", new AttributeValue{N = "0" } },
            //        {"Promotion", new AttributeValue{S = "0" } },
            //        {"Total_Play", new AttributeValue{N = "0" } },
            //        {"FirstWin", new AttributeValue{N = "0" } },
            //        {"SecondWin", new AttributeValue{N = "0" } },
            //        {"ThirdWin", new AttributeValue{N = "0" } },
            //        {"StraightWin", new AttributeValue{N = "0" } },
            //        {"LosingStreakWin", new AttributeValue{N = "0" } },
            //        {"LastConnectTime", new AttributeValue{S = DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss") } },
            //        {"HeartFreeItemTime", new AttributeValue{S = "0" } },
            //    }
            //};
            try
            {
                //var response = await db_client.PutItemAsync(request);
                //var task = new Task<Document>(() =>
                //{
                //    var response = userinfo_table.PutItem(newWrite);
                //    return response;
                //});

                // await task;

                PutItemOperationConfig config = new PutItemOperationConfig()
                {
                    ReturnValues = ReturnValues.AllNewAttributes
                };

                var response = userinfo_table.PutItemAsync(newWrite, config).ContinueWith(task =>
                {
                    return task.Result;
                });

                //var response = await userinfo_table.PutItemAsync(newWrite, config);
   
                return response.Result;
                //var response = await userinfo_table.PutItemAsync(newWrite);

                //return response.re;
            }
            catch (ConditionalCheckFailedException e)
            {
                Console.WriteLine(e);
            }
            catch (InternalServerErrorException e)
            {
                Console.WriteLine(e);
            }
            catch (ItemCollectionSizeLimitExceededException e)
            {
                Console.WriteLine(e);
            }
            catch (ProvisionedThroughputExceededException e)
            {
                Console.WriteLine(e);
            }
            catch (RequestLimitExceededException e)
            {
                Console.WriteLine(e);
            }
            catch (ResourceNotFoundException e)
            {
                Console.WriteLine(e);
            }
            catch (TransactionConflictException e)
            {
                Console.WriteLine(e);
            }
            return null;

        }

        public void Init_New_User_Info(string user_id, string nickname)
        {
            temp_user_info.user_id = "user_id";
            temp_user_info.nick_name = nickname;
            temp_user_info.tier = 3;
            temp_user_info.mmr = 1000;
            temp_user_info.heart_count = 5;
            temp_user_info.star_point = 1000;
            temp_user_info.tier_score = 0;
            temp_user_info.promotion = "0";
            temp_user_info.first_win = 0;
            temp_user_info.second_win = 0;
            temp_user_info.third_win = 0;
            temp_user_info.total_play_count = 0;
            temp_user_info.straight_win_count = 0;
            temp_user_info.losing_streak_count = 0;
            temp_user_info.last_connected_time = DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss");
            temp_user_info.heart_free_item_time = "0";
        }

        public DynamoDBClient()
        {
            AmazonDynamoDBConfig clientConfig = new AmazonDynamoDBConfig();
            // This client will access the US East 1 region.
            clientConfig.RegionEndpoint = RegionEndpoint.APNortheast2;

            db_client = new AmazonDynamoDBClient(clientConfig);

            userinfo_table = Table.LoadTable(db_client, "UserInfoTable");
            temp_user_info = new UserInfo();
        }
        
    }
}
