﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManosNet;

namespace Manos_Block_Battle_Friend_Room_Server
{
    class RoomService
    {
        MatchServerConnector match_server_connector;

        private object send_lock;
        public RoomService()
        {
            match_server_connector = new MatchServerConnector();
            match_server_connector.appcallback_on_message += on_message;
            match_server_connector.appcallback_on_status_changed += on_status_changed;
            send_lock = new object();
        }

        public void RoomManager_Server_Connect(string Address, int port)
        {
            match_server_connector.connect_server("211.250.58.163", 8989);
        }

        void on_status_changed(NETWORK_EVENT status)
        {
            switch (status)
            {
                // 접속 성공.
                case NETWORK_EVENT.connected:
                    {
                        Console.WriteLine("room manager server connected!");
                        //룸내니저에 접속하면 자신의 서버 고유 넘버 알려달라고 요청하기
                        //Packet server_num_request_msg = Packet.create((short)PROTOCOL_FRIENDSERVER_ROOMMANAGERSERVER.SERVER_NUM);
                        //Send(server_num_request_msg);
                    }
                    break;

                // 연결 끊김.
                case NETWORK_EVENT.disconnected:

                    break;

            }
        }

        void on_message(Packet msg)
        {
            PROTOCOL_FRIENDSERVER_ROOMMANAGERSERVER id = (PROTOCOL_FRIENDSERVER_ROOMMANAGERSERVER)msg.pop_protocol_id();
            switch (id)
            {
                case PROTOCOL_FRIENDSERVER_ROOMMANAGERSERVER.SERVER_NUM:
                    //룸매니저 서버로부터 자신의 서버 부여 넘버 받음
                    Program.friendServer.Set_My_Server_Num(msg.pop_int16());
                    break;

                case PROTOCOL_FRIENDSERVER_ROOMMANAGERSERVER.ADD_SERVER_INFO:
                    Program.friendServer.Add_Other_Server_Info(msg);
                    break;

                case PROTOCOL_FRIENDSERVER_ROOMMANAGERSERVER.SERVER_LIST_INFO:
                    Program.friendServer.Add_All_Server_Info_List(msg);
                    break;
            }
        }

        public void Send(Packet msg)
        {
            lock (send_lock)
            {
                match_server_connector.send(msg);
            }
        }
    }
}
