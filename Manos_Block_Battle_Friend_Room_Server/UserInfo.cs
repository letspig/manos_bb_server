﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manos_Block_Battle_Friend_Room_Server
{
    class UserInfo
    {
        public bool client_state; //false 아직 클라 준비 x, true : 클라 준비 ok
        public bool master;
        public short ready; //idle, ready, ad, no heart?
        public string user_id;
        public string nick_name;
        public short tier;//등급
        public short mmr;//mmr
        public short team_index;
        public short field_index;
        public short heart_count;
        public int star_point;
        public short tier_score;
        public string promotion; //이진수 표현? -1:노노노노노, 0: 패노노노노, 1:승노노노노, 10:승패노노노, 11:승승노노노 ..... 1100 : 승승패패노
        public int first_win;
        public int second_win;
        public int third_win;
        public int total_play_count;
        public short straight_win_count;
        public short losing_streak_count;
        public string last_connected_time;// 마지막 접속 날짜 xx년/xx월/xx일/xx시/xx분/xx초 ->데이터크기 너무 크면 int로 짤라서 보내기
        public string heart_free_item_time;//하트프리 종료 날짜, 만약에 없으면 0보내기 ->데이터크기 너무 크면 int로 짤라서 보내기

        public UserInfo()
        {

        }
        public void set_user_info(string user_id, string nick_name, short rank_score, short team_index)
        {
            this.user_id = user_id;
            this.nick_name = nick_name;
            this.mmr = rank_score;
            this.team_index = team_index;
        }

        public void reset_info()
        {
            this.user_id = "";
            this.nick_name = "";
            this.mmr = 0;
            this.team_index = 0;
        }
    }
}
