﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manos_Block_Battle_Friend_Room_Server
{
    class RoomPool
    {
        Stack<FriendRoom> m_pool;

        public RoomPool(int max_room)
        {
            m_pool = new Stack<FriendRoom>(max_room);
        }

        public void Push(FriendRoom room)
        {
            lock (m_pool)
            {
                m_pool.Push(room);
            }
        }

        public FriendRoom Pop()
        {
            lock (m_pool)
            {
                return m_pool.Pop();
            }
        }
    }
}
