﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManosNet;

namespace Manos_Block_Battle_Friend_Room_Server
{
    class FriendRoom
    {
        //유저 정보 갖고 있기
        //룸에 입장하면 다른 유저에게 자신의 정보 보내주기
        //매치 서버로 매치 요청하기
        //랭크전 아니면 레디, 스타트 갖고 있기
        //private GameLogicProcess game_logic_process;

        private List<GameUser> user_list;

        private short max_user_count = 4;
        private short current_user_count = 0;
        private Queue<short> field_index_queue;
        public short room_state; //룸 상태는 0: 비활성 1: 활성 : 2 : 게임중
        public short room_number;
        private short master_user_num;
        private GAME_MODE mode;
       

        private GameLogicProcess game_logic_process;
        
        public FriendRoom()
        {
            user_list = new List<GameUser>();
            field_index_queue = new Queue<short>();
            for(short i = 0; i < 4; i++)
            {
                field_index_queue.Enqueue(i);
            }

            game_logic_process = new GameLogicProcess();
        }

        public void Set_Room_State(short state)
        {
            room_state = state;
        }

        public short Get_Room_State()
        {
            return room_state;
        }

        public void Init(short room_number, GameUser user, GAME_MODE mode)
        {
            Reset();
            this.mode = mode;
            this.room_number = room_number;
            user.user_info.ready = 0;
            user.user_info.team_index = 0;
            user.user_info.field_index = field_index_queue.Dequeue();
            user.user_info.master = true;
            master_user_num = user.user_info.field_index;
            user_list.Add(user);
            current_user_count++;

            
        }

        public void Created_Room(GameUser user, short room_number, GAME_MODE mode)
        {
            //최초 방 생성 입장.
            Reset();
            this.mode = mode;
            this.room_number = room_number;
            user.user_info.ready = 0;
            user.user_info.team_index = 0;
            user.user_info.field_index = field_index_queue.Dequeue();
            user.user_info.master = true;
            master_user_num = user.user_info.field_index;
            user_list.Add(user);
            current_user_count++;

            game_logic_process.Init();
            game_logic_process.Add_User(user);
        }

        public void Block_Put(GameUser user, Packet msg, short field_index)
        {
            //유저가 블럭을 놨다.
            short block_num = msg.pop_int16();
            short dir = msg.pop_int16();
            short x = msg.pop_int16();
            short changed_y_value = msg.pop_int16();

            game_logic_process.Block_Put(user, changed_y_value);

            foreach(var child in user_list)
            {
                if (child.Equals(user))
                    continue;
                Packet block_msg = Packet.create((short)PROTOCOL_CLIENT_FRIENDSERVER.BLOCK);
                block_msg.push(field_index);
                block_msg.push(block_num);
                block_msg.push(dir);
                block_msg.push(x);
                child.send(block_msg);
            }
        }

        public void Send_Game_Server_Info(Packet msg)
        {
            short game_room_number = msg.pop_int16();
            string address = msg.pop_string();
            short port = msg.pop_int16();

            foreach(var child in user_list)
            {
                Packet server_info_msg = Packet.create((short)PROTOCOL_CLIENT_FRIENDSERVER.GAME_SERVER_INFO);
                server_info_msg.push(game_room_number);
                server_info_msg.push(address);
                server_info_msg.push(port);
                child.send(server_info_msg);
            }
        }

        public void EnterUser(GameUser user)
        {
            //유저 룸 입장
            //룸에 입장한 유저들에게 자신의 정보 보내주기

            if (max_user_count <= current_user_count)
                return;
            if (user_list.Contains(user))
                return;
            if (field_index_queue.Count < 1)
                return;
            user.user_info.ready = 0;
            user.user_info.team_index = 0;
            user.user_info.field_index = field_index_queue.Dequeue();
            user.user_info.master = false;
            
            user_list.Add(user);
            current_user_count++;

            game_logic_process.Init();
            game_logic_process.Add_User(user);

            Send_User_Info_To_Other_User(user);
        }

        private void Send_User_Info_To_Other_User(GameUser user)
        {
            //새로들어오면 자신의 정보 다른유저한테 보내주고

            foreach(var child in user_list)
            {
                if (child.Equals(user))
                    continue;
                if (!child.user_info.client_state)
                    continue;
                Packet msg = Packet.create((short)PROTOCOL_CLIENT_FRIENDSERVER.JOIN_USER_INFO);
                msg.push(user.user_info.field_index);
                msg.push(user.user_info.nick_name);
                msg.push(user.user_info.tier);
                msg.push(user.user_info.total_play_count);
                msg.push(user.user_info.first_win);
                msg.push(user.user_info.second_win);
                msg.push(user.user_info.third_win);
                child.send(msg);
                Console.WriteLine("join user info");
            }

            Packet enter_msg = Packet.create((short)PROTOCOL_CLIENT_FRIENDSERVER.ENTER_ROOM);
            enter_msg.push(user.user_info.field_index); //이건 보내야지
            
            user.send(enter_msg);
            //Exist_User_Info_Get(user);


        }

        public void Exist_User_Info_Get(GameUser user)
        {
            //다른 유저 정보 받아오기

            if (user_list.Count < 2)
                return;
            Packet user_info_msg = Packet.create((short)PROTOCOL_CLIENT_FRIENDSERVER.EXIST_USER_INFO);
            user_info_msg.push(master_user_num); 
            user_info_msg.push((short)(user_list.Count - 1));//유저수
            foreach (var child in user_list)
            {
                if (child.Equals(user))
                    continue;
                user_info_msg.push(child.user_info.field_index);
                user_info_msg.push(child.user_info.nick_name);
                user_info_msg.push(child.user_info.tier);
                user_info_msg.push(child.user_info.total_play_count);
                user_info_msg.push(child.user_info.first_win);
                user_info_msg.push(child.user_info.second_win);
                user_info_msg.push(child.user_info.third_win);
                user_info_msg.push(child.user_info.team_index);
                user_info_msg.push(child.user_info.ready);
                
            }
            user.send(user_info_msg);
        }


        public void User_Count_Change(GameUser user, short num)
        {
            //최대 방 입장 유저수 바꾸기
            //방장만 가능
            //현재 인원수가 최대방장수보다 많으면 변경 불가.
            if (!user.user_info.master)
                return;
            if (current_user_count > num)
                return;
            if (num > 4)
                return;

            this.max_user_count = num;
            //변경되면 변경한 응답 모든 클라에게 보내주기
            foreach(var child in user_list)
            {
                Packet msg = Packet.create((short)PROTOCOL_CLIENT_FRIENDSERVER.ROOM_USER_NUM_CHANGE);
                msg.push(num);
                child.send(msg);
            }
            
        }

        public void change_room_master()
        {
            //처음 입장한 유저가 룸 마스터
            //룸에서 나가면 두번째 유저가 룸마스터
            //해당 유저에게 룸마스터 알려주기
            //마스터 유저가 나갈 경우에만 호출됨
            
            if (user_list.Count < 1)
                return;
            //user.user_info.master = false;
            short field_index = 0;
            foreach (var child in user_list)
            {
                child.user_info.master = true;
                field_index = child.user_info.field_index;
                master_user_num = field_index;
                break;
            }
            foreach(var child in user_list)
            {
                Packet msg = Packet.create((short)PROTOCOL_CLIENT_FRIENDSERVER.ROOM_MASTER_CHANGE);
                msg.push(field_index);
                child.send(msg);
            }

        }

        public void NormalGame_Start_Request(GameUser user)
        {
            //게임 시작 요청하기
            //매치 서버로 게임 룸 생성 요청하기
            //모든 유저가 레디 상태인가
            //모든 유저가 하트가 있는가

            if (!user.user_info.master)
                return;

            foreach(var child in user_list)
            {
                if (child.Equals(user))
                    continue;

                if (child.user_info.ready != 1)
                {
                    return;
                }

                if(child.user_info.heart_count <= 0)
                {
                    return;
                }
            }

            game_logic_process.Set_Rank_Count((short)user_list.Count);

            foreach(var child in user_list)
            {
                Packet msg = Packet.create((short)PROTOCOL_CLIENT_FRIENDSERVER.NORMALGAME_START_REQUEST);
                child.send(msg);
            }

        }


        public void UserBan(GameUser user, short field_index)
        {
            //방장이 유저 벤 하기
            if (!user.user_info.master)
                return;
            //자기 자신 강퇴 ㄴㄴ
            if (user.user_info.field_index == field_index)
                return;
            //강퇴 처리는 해당 유저에게 방 나가라고 요청
            foreach(var child in user_list)
            {
                if(child.user_info.field_index == field_index)
                {
                    Packet msg = Packet.create((short)PROTOCOL_CLIENT_FRIENDSERVER.USER_BAN);
                    child.send(msg);
                    current_user_count--;
                    break;
                }
            }
        }


        public void User_State_Change(GameUser user, short state)
        {
            
            //0:idle, 1:ready, 2: ad, 3: noheart
            if(state == 1)
            {
                //heart count check
                if (user.user_info.heart_count <= 0)
                {
                    state = 3;
                    user.user_info.ready = 3;
                }
                else
                {
                    user.user_info.ready = 1;
                }
            }
            
            foreach (var child in user_list)
            {
                Packet msg = Packet.create((short)PROTOCOL_CLIENT_FRIENDSERVER.USER_STATE);
                msg.push(user.user_info.field_index);
                msg.push(state);
                child.send(msg);
            }
        }

        public void UserTeamChange(GameUser user, short team_index)
        {
            //어떤 유저가 팀을 바꿨는지
            user.user_info.team_index = team_index;
            foreach (var child in user_list)
            {

                Packet msg = Packet.create((short)PROTOCOL_CLIENT_FRIENDSERVER.TEAM_CHANGE);
                msg.push(user.user_info.field_index);
                msg.push(team_index);
                child.send(msg);
            }


        }

        public void LeaveRoom(GameUser user)
        {
            //룸에서 나가기
            //접속한 유저들에게 누가 나갔는지 알려주기
            //임의로 나갈 때 호출됨 
            //disconnect 시 호출됨

            if (user_list.Count < 2)
            {
                //혼자 있는 방이므로 다 삭제 초기화
                user.Reset();
                Room_Remove();
                return;
            }
                

            if (user_list.Contains(user))
            {
                field_index_queue.Enqueue(user.user_info.field_index);
                user_list.Remove(user);
                if (user.user_info.master)
                {
                    change_room_master();
                }
            }
            current_user_count--;

            //이미 userlist에서 삭제된 상태
            foreach (var child in user_list)
            {
                Packet msg = Packet.create((short)PROTOCOL_CLIENT_FRIENDSERVER.LEAVE_ROOM);
                msg.push(user.user_info.field_index);
                child.send(msg);
            }
        }

        public void Room_Remove()
        {
            Reset();
            Program.friendServer.Remove_Room(this);
        }

        public void Match_Cancel()
        {
            //매치서버에 매칭 취소하기 
        }

        public void Reset()
        {
            //방 상태 리셋하기
            //최대 인원 4인, 일반 모드, userlist clear, 노템모드
            //현재 접속 인원 0, field_index_queue 초기화, gamemode ,playing
            //List<GameUser> user_list;

            //short max_user_count = 4;
            //short current_user_count = 0;
            //Queue<short> field_index_queue;
            //GAME_MODE game_mode;
            //bool playing;
            //short room_number;
            //short master_user_num;

            user_list.Clear();
            current_user_count = 0;
            room_state = 0;
            field_index_queue.Clear();
            for (short i = 0; i < 4; i++)
            {
                field_index_queue.Enqueue(i);
            }

        }
    }
}
