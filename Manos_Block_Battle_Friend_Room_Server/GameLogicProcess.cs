﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManosNet;

namespace Manos_Block_Battle_Friend_Room_Server
{
    class GameLogicProcess
    {
        //매번 팀인덱스 검사해서 처리하는것도 일이다. 지정해 놓자.
        private List<GameUser> user_list;

        private Stack<FieldLogicProcess> field_logic_stack;

        private List<GameUser> lose_user_list;
        private List<GameUser> live_user_list;

        private short max_user_count = 4;
        private short current_user_count = 0;
        private short rank_count = 4;

        public void Attack_Line(short field_index, short team_index, short attack_line)
        {
            //내가 다른 유저들 공격(나와 팀이 다른경우 혹은 내가 솔로팀인 경우)
            //게임중인 플레이어한테만 공격 가능
            //나와 다른 팀 필드 로직을 미리 등록을 해야 하는가?
            //내가 솔로팀이면 나머지 전부 공격
            //내가 레드 또는 블루면 솔로 또는 레드불에 공격

            foreach(var child in user_list)
            {
                if (child.Get_User_Info().field_index == field_index)
                    continue;
                if(team_index == child.Get_User_Info().team_index)
                {
                    if (team_index != 0)
                        continue;
                }

                child.Get_FieldLogic().Increase_Top_Y(attack_line);

                if (child.Get_FieldLogic().Game_End_Check())
                {
                    lose_user_list.Add(child);
                    live_user_list.Remove(child);
                }
            }

            if(lose_user_list.Count > 0)
            {
                //패배 처리
                foreach(var child in lose_user_list)
                {
                    Packet rank_msg = Packet.create((short)PROTOCOL_CLIENT_FRIENDSERVER.GAME_RESULT);
                    rank_msg.push(child.Get_User_Info().field_index);
                    rank_msg.push(rank_count);
                    child.send(rank_msg);
                }

                rank_count--;
            }

            if (live_user_list.Count == 1)
            {
                //완전 게임 종료처리 1등 한테 보내기 
                foreach (var first_child in live_user_list)
                {
                    foreach (var child in user_list)
                    {
                        Packet rank_msg = Packet.create((short)PROTOCOL_CLIENT_FRIENDSERVER.GAME_RESULT);
                        rank_msg.push(first_child.Get_User_Info().field_index);
                        rank_msg.push(rank_count);
                        child.send(rank_msg);
                    }
                }

            }


        }

        public void Block_Put(GameUser user, short changed_y_value)
        {
            //내 블럭 놓기
            FieldLogicProcess field_logic = user.Get_FieldLogic();
            if (!field_logic.Get_Playing())
            {
                return;
            }
            field_logic.Change_Top_Y_Value(changed_y_value);
            if (field_logic.Game_End_Check())
            {
                //게임 패배
                //랭크 계산해서 패배 처리
                foreach (var child in user_list)
                {
                    Packet rank_msg = Packet.create((short)PROTOCOL_CLIENT_FRIENDSERVER.GAME_RESULT);
                    rank_msg.push(user.Get_User_Info().field_index);
                    rank_msg.push(rank_count);
                    child.send(rank_msg);
                }
                live_user_list.Remove(user);
                rank_count--;
            }

            if(live_user_list.Count == 1)
            {

                foreach(var first_child in live_user_list)
                {
                    foreach (var child in user_list)
                    {
                        Packet rank_msg = Packet.create((short)PROTOCOL_CLIENT_FRIENDSERVER.GAME_RESULT);
                        rank_msg.push(first_child.Get_User_Info().field_index);
                        rank_msg.push(rank_count);
                        child.send(rank_msg);
                    }
                }

            }

        }

        public void Leave_User(GameUser user)
        {
            if (user_list.Contains(user))
            {
                user_list.Remove(user);
                field_logic_stack.Push(user.Get_FieldLogic());
            }
                
        }
        public void Add_User(GameUser user)
        {
            if (user_list.Contains(user))
                return;
            //필드로직스택에서 하나 빼와서 셋
            if (current_user_count >= max_user_count)
                return;
            current_user_count++;
            if (field_logic_stack.Count < 1)
                return;
            user.Set_FieldLogic(field_logic_stack.Pop());

            live_user_list.Add(user);

        }

        public void Set_Rank_Count(short count)
        {
            rank_count = count;
        }

        public void Init()
        {
            //게임 시작 전에 리셋하고 시작 
            Reset();
        }

        public void Reset()
        {
            //게임 시작전 또는 재시작시 리셋
            current_user_count = 0;
            user_list.Clear();
            lose_user_list.Clear();
            live_user_list.Clear();
        }
        public GameLogicProcess()
        {
            //기본 정보 셋
            user_list = new List<GameUser>();
            lose_user_list = new List<GameUser>();
            live_user_list = new List<GameUser>();
            field_logic_stack = new Stack<FieldLogicProcess>();
            for(int i = 0; i < 4; i++)
            {
                field_logic_stack.Push(new FieldLogicProcess());
            }
        }
    }
}
