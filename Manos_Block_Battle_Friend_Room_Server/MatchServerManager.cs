﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManosNet;

namespace Manos_Block_Battle_Friend_Room_Server
{
    class MatchServerManager
    {
        //매치서버 갯수 만큼 커넥션 생성
        //어느 매치 서버로 매치 요청 또는 룸생성 보낼지 결정
        //일단 1:1매치, 2:2매치, 일반게임 총 3개 커넥션 같은 서버에 포트로 구분해서 연결
        private MatchService match_service;
        //private int Match_Server_Count = 3;
        public MatchServerManager()
        {

            match_service = new MatchService();
        }

        public void Init()
        {
            //match_service.Match_Server_Connect("211.250.58.163", 7979);
        }

        

        public void Match_Request(PROTOCOL_FRIENDSERVER_MATCH match_type, Packet msg)
        {
            switch (match_type)
            {
                case PROTOCOL_FRIENDSERVER_MATCH.Normal_Game_Request:
                    match_service.Send(msg);
                    break;

                case PROTOCOL_FRIENDSERVER_MATCH.One_One_Match_Request:

                    break;

                case PROTOCOL_FRIENDSERVER_MATCH.One_One_Match_Item_Request:

                    break;
                case PROTOCOL_FRIENDSERVER_MATCH.Two_Two_Match_Request:

                    break;
                case PROTOCOL_FRIENDSERVER_MATCH.Two_Two_Match_Item_Request:

                    break;
                case PROTOCOL_FRIENDSERVER_MATCH.Two_Two_Match_Solo_Request:

                    break;
                case PROTOCOL_FRIENDSERVER_MATCH.Two_Two_Match_Solo_Item_Request:

                    break;
                case PROTOCOL_FRIENDSERVER_MATCH.One_One_One_Match_Request:

                    break;
                case PROTOCOL_FRIENDSERVER_MATCH.One_One_One_Match_Item_Request:

                    break;
                case PROTOCOL_FRIENDSERVER_MATCH.One_One_One_One_Match_Request:

                    break;
                case PROTOCOL_FRIENDSERVER_MATCH.One_One_One_One_Match_Item_Request:

                    break;
            }
        }
    }
}
