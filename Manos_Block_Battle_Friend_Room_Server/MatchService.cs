﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManosNet;

namespace Manos_Block_Battle_Friend_Room_Server
{
    class MatchService
    {
        MatchServerConnector match_server_connector;

        private object send_lock;
        public MatchService()
        {
            match_server_connector = new MatchServerConnector();
            match_server_connector.appcallback_on_message += on_message;
            match_server_connector.appcallback_on_status_changed += on_status_changed;
            send_lock = new object();
        }

        public void Match_Server_Connect(string Address, int port)
        {
            match_server_connector.connect_server(Address, port);
        }

        void on_status_changed(NETWORK_EVENT status)
        {
            switch (status)
            {
                // 접속 성공.
                case NETWORK_EVENT.connected:
                    {
                        Console.WriteLine("match_server connected!");
                    }
                    break;

                // 연결 끊김.
                case NETWORK_EVENT.disconnected:

                    break;

            }
        }

        void on_message(Packet msg)
        {
            //this.message_receiver.SendMessage("on_recv", msg);
            PROTOCOL_FRIENDSERVER_MATCH id = (PROTOCOL_FRIENDSERVER_MATCH)msg.pop_protocol_id();
            switch (id)
            {
                case PROTOCOL_FRIENDSERVER_MATCH.CREATE_ROOM_COMPLETE:
                    //룸생성 완료되었으니 룸번호로 프렌드룸 찾아서 게임 서버 정보 보내주자
                    Program.friendServer.Find_Room_And_Send(msg);
                    break;
            }
        }

        public void Send(Packet msg)
        {
            lock (send_lock)
            {
                match_server_connector.send(msg);
            }
        }
    }
}
