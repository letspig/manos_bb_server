﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManosNet;

namespace Manos_Block_Battle_Friend_Room_MGR_Server
{
    //프렌드서버
    //모든 프렌드 룸 번호 관리
    
    
    class Program
    {
        public static FriendServerManager friendServerManager = new FriendServerManager();

        static void Main(string[] args)
        {
            PacketBufferManager.initialize(2000);

            NetworkService networkService = new NetworkService();


            networkService.session_created_callback += on_session_created_friend_server;
            networkService.initialize();
            networkService.listen("0.0.0.0", 7979, 100);

            NetworkService lobby_server_networkService = new NetworkService();



            lobby_server_networkService.session_created_callback += on_session_created_lobby_server;
            lobby_server_networkService.initialize();
            lobby_server_networkService.listen("0.0.0.0", 9090, 100);

            Console.WriteLine("Server Start");

            while (true)
            {
                System.Threading.Thread.Sleep(1000);
            }
        }

        static void on_session_created_friend_server(UserToken token)
        {
            //프렌드서버 새로 접속하면 매니저에 등록
            friendServerManager.Add_Friend_Server(new FriendServer(token));
            Console.WriteLine("friend_server session created.");
        }


        static void on_session_created_lobby_server(UserToken token)
        {
            Console.WriteLine("lobby server session created.");
        }
    }
}
