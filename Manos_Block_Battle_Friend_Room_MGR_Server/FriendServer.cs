﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManosNet;
using System.Net;

namespace Manos_Block_Battle_Friend_Room_MGR_Server
{
    class FriendServer :IPeer
    {
        UserToken token;
        private Dictionary<short, Server_Info> server_info_dic;
        private short friend_server_number;
        public string address;
        public int port;
        public FriendServer(UserToken token)
        {
            this.token = token;
            this.token.set_peer(this);
            address = ((IPEndPoint)(token.Get_Socket().RemoteEndPoint)).Address.ToString();
            port = ((IPEndPoint)(token.Get_Socket().RemoteEndPoint)).Port;
            server_info_dic = new Dictionary<short, Server_Info>();
        }

        //프렌드서버 고유 넘버
        public void Set_Friend_Server_Number(short number)
        {
            friend_server_number = number;
        }

        public void Add_Server_Info(short server_num, Server_Info info)
        {
            lock (server_info_dic)
            {
                server_info_dic.Add(server_num, info);
            }  
        }

        void IPeer.on_message(byte[] buffer)
        {
            //byte[] clone = new byte[1024];
            //Array.Copy(buffer, clone, buffer.Length);
            //Packet msg = new Packet(clone, this);
            //Program.friendServerManager.Enqueue_Packet(msg);
            Packet msg = new Packet(buffer, this);
            PROTOCOL_FRIENDSERVER_ROOMMANAGERSERVER protocol = (PROTOCOL_FRIENDSERVER_ROOMMANAGERSERVER)msg.pop_protocol_id();
            Console.WriteLine("------------------------------------------------------");
            Console.WriteLine("protocol id " + protocol);
            switch (protocol)
            {
                case PROTOCOL_FRIENDSERVER_ROOMMANAGERSERVER.SERVER_NUM:
                    Packet server_num_msg = Packet.create((short)PROTOCOL_FRIENDSERVER_ROOMMANAGERSERVER.SERVER_NUM);
                    server_num_msg.push(friend_server_number);
                    send(server_num_msg);
                    break;

                case PROTOCOL_FRIENDSERVER_ROOMMANAGERSERVER.FIND_SERVER:
                    //해당 서버가 어디인지 알려주세요
                    short server_num = msg.pop_int16();//서버 번호
                    
                    Packet response = Packet.create((short)PROTOCOL_FRIENDSERVER_ROOMMANAGERSERVER.SERVER_ADDRESS);
                    response.push(server_info_dic[server_num].address);

                    send(response);
                    break;


            }
        }

        void IPeer.on_removed()
        {
            Console.WriteLine("The client disconnected.");
        }

        public void send(Packet msg)
        {
            this.token.send(msg);
        }

        void IPeer.disconnect()
        {
            this.token.Get_Socket().Disconnect(false);
        }

        void IPeer.process_user_operation(Packet msg)
        {
            
        }
    }
}
