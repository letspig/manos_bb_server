﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManosNet;
using System.Threading;

namespace Manos_Block_Battle_Friend_Room_MGR_Server
{
    class FriendServerManager
    {
        private object operation_lock;
        private Queue<Packet> user_operations;
        private Thread logic_Thread;
        private AutoResetEvent loop_event;

        private Dictionary<int, FriendServer> friend_server_dic;
        private List<Server_Info> server_info_list;

        private Queue<short> server_number_queue;
        private short Server_Start_Number = 1000;
        
        public FriendServerManager()
        {
            operation_lock = new object();
            loop_event = new AutoResetEvent(false);
            user_operations = new Queue<Packet>();

            friend_server_dic = new Dictionary<int, FriendServer>();
            server_info_list = new List<Server_Info>();
            server_number_queue = new Queue<short>();

            for(short i = Server_Start_Number; i <= 9999; i++)
            {
                server_number_queue.Enqueue(i);
            }

            logic_Thread = new Thread(gameLoop);
            logic_Thread.Start();
        }

        public void Add_Friend_Server(FriendServer server)
        {
            //새로운 서버가 생길때마다 모든 서버에게 서버 번호당 서버 주소 알려주기
            short server_num = server_number_queue.Dequeue();
            server.Set_Friend_Server_Number(server_num);
            lock (friend_server_dic)
            {
                Server_Info server_info = new Server_Info(server.address, server_num);
                //이미  접속한 서버들한테 새로 추가된 서버 정보 보내주기
                foreach (var child in friend_server_dic)
                {
                    Packet add_server_info = Packet.create((short)PROTOCOL_FRIENDSERVER_ROOMMANAGERSERVER.ADD_SERVER_INFO);
                    add_server_info.push(server_num);
                    add_server_info.push(server_info.address);
                    //add_server_info.push(server_info.port);
                    child.Value.send(add_server_info);
                }

                friend_server_dic.Add(server_num, server);
                server_info_list.Add(server_info);

                //foreach (var child in friend_server_dic)
                //{
                //    child.Value.Add_Server_Info(server_num, server_info);
                //}

            }
            //접속한 서버에게 자신의 정보 보내주기
            Packet server_num_msg = Packet.create((short)PROTOCOL_FRIENDSERVER_ROOMMANAGERSERVER.SERVER_NUM);
            server_num_msg.push(server_num);
            server.send(server_num_msg);

            //접속한 서버에게 현재 있는 서버 리스트 전부 보내주기
            Packet server_info_list_msg = Packet.create((short)PROTOCOL_FRIENDSERVER_ROOMMANAGERSERVER.SERVER_LIST_INFO);
            server_info_list_msg.push((short)server_info_list.Count); //서버 몇개인지
            foreach (var child in server_info_list)
            {
                server_info_list_msg.push(child.server_num);
                server_info_list_msg.push(child.address);
                //server_info_list_msg.push(child.port);
            }

            server.send(server_info_list_msg);
        }

        private void gameLoop()
        {
            while (true)
            {
                Packet packet = null;
                lock (operation_lock)
                {
                    if (user_operations.Count > 0)
                    {
                        packet = user_operations.Dequeue();
                    }
                }

                if (packet != null)
                {
                    //packet 처리
                    process_receive(packet);
                }

                if (user_operations.Count <= 0)
                {
                    loop_event.WaitOne();
                }
            }
        }

        public void Enqueue_Packet(Packet packet)
        {
            lock (operation_lock)
            {
                user_operations.Enqueue(packet);
                loop_event.Set();
            }
        }

        private void process_receive(Packet packet)
        {
            packet.owner.process_user_operation(packet);
        }
    }
}
