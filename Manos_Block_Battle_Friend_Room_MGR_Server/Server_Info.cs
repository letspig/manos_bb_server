﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manos_Block_Battle_Friend_Room_MGR_Server
{
    class Server_Info
    {
        public string address;
        //public short port;
        public short server_num;

        public Server_Info(string address, short server_num)
        {
            this.address = address;
            //this.port = port;
            this.server_num = server_num;
        }
    }
}
