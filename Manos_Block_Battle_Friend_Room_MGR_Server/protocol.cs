﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manos_Block_Battle_Friend_Room_MGR_Server
{
    public enum PROTOCOL_FRIENDSERVER_ROOMMANAGERSERVER : short
    {
        SERVER_NUM,
        FIND_SERVER,
        ADD_SERVER_INFO,
        SERVER_LIST_INFO,
        SERVER_ADDRESS,
        ROOM_DELETE,
        END
    }
}
