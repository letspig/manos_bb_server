﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManosNet;

namespace Manos_Block_Battle_Login_Server
{
    class MatchServer
    {
        private List<GameUser> matching_user_list;
        private int room_count = 0;

        public MatchServer()
        {
            matching_user_list = new List<GameUser>();
        }

        public void match_req(GameUser user)
        {
            if (matching_user_list.Contains(user))
            {
                return;
            }
            matching_user_list.Add(user);

            if(matching_user_list.Count == 2)
            {
                //match가 잡히면 게임서버에 룸번호와 함께 방 생성 요청 
                //create game room request to gameserver
                //Packet response = Packet.create((short)PROTOCOL.Match_Success);
                //response.push(room_count);
                //matching_user_list[0].send(response);
                //matching_user_list[1].send(response);
                //matching_user_list.Clear();
                //room_count++;
            }
        }
    }
}
