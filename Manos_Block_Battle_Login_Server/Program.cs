﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManosNet;
using System.Net;

namespace Manos_Block_Battle_Login_Server
{
    class Program
    {
        static List<GameUser> userlist;
        public static MatchServer match_server = new MatchServer();
        static void Main(string[] args)
        {
            
            NetworkService networkService = new NetworkService();
            PacketBufferManager.initialize(2000);

            userlist = new List<GameUser>();

            networkService.session_created_callback += on_session_created;
            networkService.initialize();
            networkService.listen("0.0.0.0", 7979, 100);

            Console.WriteLine("Server Start");

            //Connector connector = new Connector(networkService);
            //connector.connected_callback += connected_gameserver;
            //IPEndPoint iPEndPoint = new IPEndPoint(IPAddress.Parse("211.250.58.163"), 8989);
            //connector.connect(iPEndPoint);

            while (true)
            {
                System.Threading.Thread.Sleep(1000);
            }

        }

        static void on_session_created(UserToken token)
        {
            Console.WriteLine("session created.");
            GameUser user = new GameUser(token);
            lock (userlist)
            {
                userlist.Add(user);
            }
        }

        public static void remove_user(GameUser user)
        {
            lock (userlist)
            {
                userlist.Remove(user);
            }
        }

        public static void connected_gameserver(UserToken userToken)
        {

        }
    }
}
