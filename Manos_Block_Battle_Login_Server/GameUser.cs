﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManosNet;

namespace Manos_Block_Battle_Login_Server
{
    class GameUser : IPeer
    {
        UserToken token;

        public GameUser(UserToken token)
        {
            this.token = token;
            this.token.set_peer(this);
        }

        void IPeer.on_message(byte[] buffer)
        {
            // ex)
            
            Packet msg = new Packet(buffer, this);
            PROTOCOL protocol = (PROTOCOL)msg.pop_protocol_id();
            Console.WriteLine("------------------------------------------------------");
            Console.WriteLine("protocol id " + protocol);
            switch (protocol)
            {
                case PROTOCOL.LOGIN:
                    {
                        string text = msg.pop_string();
                        Console.WriteLine(string.Format("text {0}", text));

                        Packet response = Packet.create((short)PROTOCOL.LOGIN_SUCCESS);
                        response.push(text);
                        send(response);
                        break;
                    }

                case PROTOCOL.One_One_Match_Request:
                    Program.match_server.match_req(this);
                    break;

            }
        }

        void IPeer.on_removed()
        {
            Console.WriteLine("The client disconnected.");

            Program.remove_user(this);
        }

        public void send(Packet msg)
        {
            this.token.send(msg);
        }

        void IPeer.disconnect()
        {
            this.token.Get_Socket().Disconnect(false);
        }

        void IPeer.process_user_operation(Packet msg)
        {
        }
    }
}
