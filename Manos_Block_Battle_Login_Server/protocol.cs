﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manos_Block_Battle_Login_Server
{
    public enum PROTOCOL : short
    {
        BEGIN = 0,
        LOGIN,
        LOGIN_SUCCESS,
        One_One_Match_Request,
        Match_Cancel,
        Match_Success,
        END
    }
}
