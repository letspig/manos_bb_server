﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace ManosNet
{
    public class Connector
    {
        public delegate void ConnectedHandler(UserToken token);
        public ConnectedHandler connected_callback { get; set; }

        // 원격지 서버와의 연결을 위한 소켓.
        Socket client;

        NetworkService network_service;

        public Connector(NetworkService network_service)
        {
            this.network_service = network_service;
            this.connected_callback = null;
        }

        public void close_socket()
        {
            if(this.client.Connected)
                this.client.Close();
        }

        public void connect(IPEndPoint remote_endpoint)
        {
            this.client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            // 비동기 접속을 위한 event args.
            SocketAsyncEventArgs event_arg = new SocketAsyncEventArgs();
            event_arg.Completed += on_connect_completed;
            event_arg.RemoteEndPoint = remote_endpoint;
            bool pending = this.client.ConnectAsync(event_arg);
            if (!pending)
            {
                on_connect_completed(null, event_arg);
            }
        }

        void on_connect_completed(object sender, SocketAsyncEventArgs e)
        {
            if (e.SocketError == SocketError.Success)
            {
                //Console.WriteLine("Connect completd!");
                UserToken token = new UserToken();

                // 데이터 수신 준비.
                this.network_service.on_connect_complete(this.client, token);

                if (this.connected_callback != null)
                {
                    this.connected_callback(token);
                }
            }
            else
            {
                // failed.
                Console.WriteLine(string.Format("Failed to connect. {0}", e.SocketError));
            }
        }
    }
}
