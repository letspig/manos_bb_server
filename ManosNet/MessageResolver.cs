﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManosNet
{
    class Defines
    {
        public static readonly short HEADERSIZE = 2;
    }
    public class MessageResolver
    {
        byte[] message_buffer = new byte[1024];
        // 메시지 사이즈.
        int message_size;


        // 현재 진행중인 버퍼의 인덱스를 가리키는 변수.
        // 패킷 하나를 완성한 뒤에는 0으로 초기화 시켜줘야 한다.
        int current_position;

        // 읽어와야 할 목표 위치.
        int position_to_read;

        // 남은 사이즈.
        int remain_bytes;

        public MessageResolver()
        {
            this.message_size = 0;
            this.current_position = 0;
            this.position_to_read = 0;
            this.remain_bytes = 0;
        }
        public delegate void CompletedMessageCallback(byte[] buffer);
        public void on_receive(byte[] buffer, int offset, int transffered, CompletedMessageCallback callback)
        {
            //헤더부터 읽는다(헤더의 크기는 2byte로 고정)
            //헤더를 모두 읽으면 데이터의 크기를 가져온다.
            //데이터의 크기만큼 읽는다.
            //완료되면 데이터를 넘겨준다.

            remain_bytes = transffered;
            int src_position = offset;

            while (remain_bytes > 0)
            {
                bool completed = false;

                //head를 아직 전부 읽지 않았다.
                if (current_position < Defines.HEADERSIZE)
                {
                    this.position_to_read = Defines.HEADERSIZE;

                    completed = read_until(buffer, ref src_position, offset, transffered);
                    if (!completed)
                    {
                        // 아직 다 못읽었으므로 다음 receive를 기다린다.
                        return;
                    }

                    // 헤더 하나를 온전히 읽어왔으므로 메시지 사이즈를 구한다.
                    this.message_size = get_body_size();

                    // 다음 목표 지점(헤더 + 메시지 사이즈).
                    this.position_to_read = this.message_size + Defines.HEADERSIZE;
                }

                // 메시지를 읽는다.
                completed = read_until(buffer, ref src_position, offset, transffered);

                if (completed)
                {
                    // 패킷 하나를 완성 했다.
                    callback(message_buffer);

                    clear_buffer();
                }

            }
        }

        private bool read_until(byte[] buffer, ref int src_position, int offset, int transffered)
        {
            if (this.current_position >= offset + transffered)
            {
                // 들어온 데이터 만큼 다 읽은 상태이므로 더이상 읽을 데이터가 없다.
                return false;
            }

            int copy_size = this.position_to_read - current_position;
            if (copy_size > remain_bytes)
                copy_size = remain_bytes;

            // 버퍼에 복사.
            Array.Copy(buffer, src_position, this.message_buffer, this.current_position, copy_size);


            // 원본 버퍼 포지션 이동.
            src_position += copy_size;

            // 타겟 버퍼 포지션도 이동.
            this.current_position += copy_size;

            // 남은 바이트 수.
            this.remain_bytes -= copy_size;

            // 목표지점에 도달 못했으면 false
            if (this.current_position < this.position_to_read)
            {
                return false;
            }

            return true;
        }

        int get_body_size()
        {
            // 헤더 타입의 바이트만큼을 읽어와 메시지 사이즈를 리턴한다.

            Type type = Defines.HEADERSIZE.GetType();
            if (type.Equals(typeof(Int16)))
            {
                return BitConverter.ToInt16(this.message_buffer, 0);
            }

            return BitConverter.ToInt32(this.message_buffer, 0);
        }

        void clear_buffer()
        {
            Array.Clear(this.message_buffer, 0, this.message_buffer.Length);

            this.current_position = 0;
            this.message_size = 0;

        }
    }
}
