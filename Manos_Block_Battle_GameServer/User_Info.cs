﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manos_Block_Battle_GameServer
{
    class User_Info
    {
        public string user_id;
        public string nick_name;
        public short rank_score;
        public short team_index;
        public short field_index;
        public short ready;
        public User_Info()
        {

        }
        public void set_user_info(string user_id, string nick_name, short rank_score, short team_index, short field_index)
        {
            this.user_id = user_id;
            this.nick_name = nick_name;
            this.rank_score = rank_score;
            this.team_index = team_index;
            this.field_index = field_index;
        }

        public void reset_info()
        {
            this.user_id = "";
            this.nick_name = "";
            this.rank_score = 0;
            this.team_index = 0;
            this.field_index = 0;
        }
    }
}
