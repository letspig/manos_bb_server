﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manos_Block_Battle_GameServer
{
    //최대 게임 룸 1000개
    //비사용중인 룸 찾아서 내줄것
    //사용중인 룸 갯수 변수로 저장 해두고 다음 룸 줄것
    class RoomPool
    {
        Stack<GameRoom> m_pool;

        public RoomPool(int max_room)
        {
            m_pool = new Stack<GameRoom>(max_room);
        }

        public void Push(GameRoom room)
        {
            lock (m_pool)
            {
                m_pool.Push(room);
            }
        }

        public GameRoom Pop()
        {
            lock (m_pool)
            {
                return m_pool.Pop();
            }
        }
    }
}
