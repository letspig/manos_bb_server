﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manos_Block_Battle_GameServer.Block
{
    class Block_8 : Block_Parent
    {
        //o
        //o
        public Block_8()
        {
            max_rotation_count = 2;
            pos_list = new List<Pos>();
            m_pos = new Pos[2];
            for (int i = 0; i < 2; i++)
            {
                m_pos[i] = new Pos(0, 0);
            }
        }

        override public List<Pos> Calc(short x, short y, short rotation)
        {
            if (0 > rotation || rotation >= pos_list.Count)
                return null;
            pos_list.Clear();

            switch (rotation)
            {
                case 0:
                    m_pos[0].setPos(x, y);
                    m_pos[1].setPos(x, y - 1);
                    break;

                case 1:
                    m_pos[0].setPos(x, y);
                    m_pos[1].setPos(x + 1, y);
                    break;

            }

            AddPos();
            return pos_list;
        }
    }
}
