﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manos_Block_Battle_GameServer.Block
{
    class Block_Parent
    {
        //기준 좌표(x,y)와 rotation값이 주어졌을 때 각 브릭들 좌표(x,y)값을 계산한다.

        protected List<Pos> pos_list;
        protected Pos[] m_pos;
        protected short max_rotation_count;
        public Block_Parent()
        {
            //pos_list = new List<Pos>();
        }

        public short Get_Max_Rotation_Count()
        {
            return max_rotation_count;
        }

        virtual public List<Pos> Calc(short x, short y, short rotation)
        {
            if (0 > rotation || rotation >= pos_list.Count)
                return null;
            return pos_list;
        }

        protected void AddPos()
        {
            for(int i = 0; i < m_pos.Length; i++)
            {
                pos_list.Add(m_pos[i]);
            }
        }
    }
}
