﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manos_Block_Battle_GameServer.Block
{
    class Block_7 : Block_Parent
    {
        //o
        //o
        //o
        //o
        //o
        public Block_7()
        {
            max_rotation_count = 2;
            pos_list = new List<Pos>();
            m_pos = new Pos[5];
            for (int i = 0; i < 5; i++)
            {
                m_pos[i] = new Pos(0, 0);
            }
        }

        override public List<Pos> Calc(short x, short y, short rotation)
        {
            if (0 > rotation || rotation >= pos_list.Count)
                return null;
            pos_list.Clear();

            switch (rotation)
            {
                case 0:
                    m_pos[0].setPos(x, y);
                    m_pos[1].setPos(x, y - 1);
                    m_pos[2].setPos(x, y - 2);
                    m_pos[3].setPos(x, y + 1);
                    m_pos[4].setPos(x, y - 3);
                    break;

                case 1:
                    m_pos[0].setPos(x, y);
                    m_pos[1].setPos(x + 1, y);
                    m_pos[2].setPos(x + 2, y);
                    m_pos[3].setPos(x + 3, y);
                    m_pos[4].setPos(x, y - 1);
                    break;

            }

            AddPos();
            return pos_list;
        }
    }
}
