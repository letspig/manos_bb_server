﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manos_Block_Battle_GameServer.Block
{
    //oo
    //oo
    class Block_0 : Block_Parent
    {
        
        public Block_0()
        {
            max_rotation_count = 1;
            pos_list = new List<Pos>();
            m_pos = new Pos[4];
            for(int i = 0; i < 4; i++)
            {
                m_pos[i] = new Pos(0, 0);
            }
        }

        override public List<Pos> Calc(short x, short y, short rotation)
        {
            if (0 > rotation || rotation >= pos_list.Count)
                return null;

            pos_list.Clear();
            m_pos[0].setPos(x, y);
            m_pos[1].setPos(x + 1, y);
            m_pos[2].setPos(x, y - 1);
            m_pos[3].setPos(x + 1, y - 1);


            AddPos();
            return pos_list;
        }


    }
}
