﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Manos_Block_Battle_GameServer.Block;

namespace Manos_Block_Battle_GameServer
{
    class Game_Field
    {
        //블럭 클래스 10개 배열(타입 순서대로)
        private Block_Parent []Blocks;
        private short[] Refill_Blocks;
        private bool[][] m_brick_fill;
        private List<int> LineClear_Y_List;
        private int empty_count = 0;
        private int combo_count = 0;
        private int line_clear_count = 0;
        private int attack_count = 0;
        private int brick_empty_count = 0;
        private int line_up_empty_x = 0;

        private Well_Random w_random; //시드 받아와서 정해진 랜덤
        private Well_Random w_random_line_up;
        private int field_width_count = 10;
        private int field_height_count = 15;

        private User_Info user_info;
        public short used_block_type;

        public bool playing;
        public short game_result; //순위 4,3,2,1
        public bool idle;// 블럭을 놓을수 없는 상태(게임이 끝나건 아님)

        string user_id, nickname;
        short rank_score, team_index;

        public Game_Field()
        {
            Blocks = new Block_Parent[10];
            Blocks[0] = new Block_0();
            Blocks[1] = new Block_1();
            Blocks[2] = new Block_2();
            Blocks[3] = new Block_3();
            Blocks[4] = new Block_4();
            Blocks[5] = new Block_5();
            Blocks[6] = new Block_6();
            Blocks[7] = new Block_7();
            Blocks[8] = new Block_8();
            Blocks[9] = new Block_9();

            Refill_Blocks = new short[3];

            m_brick_fill = new bool[field_width_count][];
            for(int i = 0; i < field_width_count; i++)
            {
                m_brick_fill[i] = new bool[field_height_count];
                for(int j = 0; j < field_height_count; j++)
                {
                    m_brick_fill[i][j] = false;
                }
            }

            LineClear_Y_List = new List<int>();
            w_random = new Well_Random();
            w_random_line_up = new Well_Random();
            user_info = new User_Info();
        }

        public void Set_User_Info(short team_index)
        {
            user_info.team_index = team_index;
        }

        public void Set_User_Info(string user_id, string nick_name, short rank_score, short team_index)
        {
            user_info.user_id = user_id;
            user_info.nick_name = nickname;
            user_info.rank_score = rank_score;
            user_info.team_index = team_index;
        }

        public User_Info Get_User_Info()
        {
            return user_info;
        }

        public void init(uint block_seed)
        {
            w_random.Set_Seed(block_seed);
            w_random_line_up.Set_Seed(block_seed);
            Block_Refill(0);
            Block_Refill(1);
            Block_Refill(2);
        }

        private void Block_Refill(int index)
        {
            Refill_Blocks[index] = (short)w_random.Next(0, 10);
        }

        private bool Block_Empty_Check(int x, int y)
        {
            if(0 <= x && x < 10)
            {
                if(0 <= y && y < 14)
                {
                    if (!m_brick_fill[x][y])
                        return false;//비어있다
                }
            }
            return true;//안비거나 x,y범위가 안맞는다
        }

        public void Block_Field_On(int refill_num, short x, short y, short rotation)
        {
            attack_count = 0;
            line_clear_count = 0;
            brick_empty_count = 0;
            LineClear_Y_List.Clear();
            //블럭 타입과 로테이션으로 x,y를 기준으로 brick type 을 바꾼다.
            //클라이언트와 동기화를 위해서 x,y기준에서 현재 상태의 가장 아래쪽 y값에 블럭을 위치한다.
            List<Pos> pos_list;

            for(short i = 0; i < field_height_count; i++)
            {
                if (!m_brick_fill[x][i])
                {
                    brick_empty_count = 0;
                    //해당 x,i위치에서 블럭을 놓을수 있는지 체크한다.
                    pos_list = Blocks[Refill_Blocks[refill_num]].Calc(x, i, rotation);
                    if (pos_list == null)
                        return;
                    foreach (var child in pos_list)
                    {
                        if (!Block_Empty_Check(child.x, child.y))
                            brick_empty_count++;
                        else
                        {
                            break;
                        }
                    }
                    if(brick_empty_count == pos_list.Count)
                    {
                        foreach (var child in pos_list)
                        {
                            m_brick_fill[child.x][child.y] = true;
                            LineClear_Y_List.Add(child.y);
                        }
                        break;
                    }
                    else
                    {
                        foreach (var child in pos_list)
                        {
                            if(child.y > 14)
                            {
                                //게임 패배시키기
                                playing = false;
                                break;
                            }
                            
                        }
                    }
                }
            }

            

            //line clear check
            LineClear_Y_List.Sort();
            LineClear_Y_List.Reverse();
            
            foreach(var child in LineClear_Y_List)
            {
                for(int i = 0; i < field_width_count; i++)
                {
                    if (m_brick_fill[i][child])
                    {
                        empty_count++;
                    }
                }
                if(empty_count == field_width_count)
                {
                    //lineclear
                    line_clear_count++;
                    for (int i = child; i < field_height_count; i++)
                    {
                        for(int j = 0; j < field_width_count; j++)
                        {
                            if(i == field_height_count - 1)
                            {
                                m_brick_fill[j][i] = false;
                            }
                            else
                            {
                                m_brick_fill[j][i] = m_brick_fill[j][i + 1];
                            }
                        }
                    }
                }

                empty_count = 0;
            }

            if (line_clear_count > 0)
            {
                if(line_clear_count == 3)
                {
                    attack_count++;
                }
                else if(line_clear_count == 4)
                {
                    attack_count += 3;
                }

                else if(line_clear_count == 5)
                {
                    attack_count += 4;
                }
                combo_count++;
                if(combo_count > 2)
                {
                    attack_count++;
                }
                line_up_empty_x = (int)w_random_line_up.Next(0, 10);
            }
            else
            {
                combo_count = 0;
            }
            used_block_type = Refill_Blocks[refill_num];
            Block_Refill(refill_num);
            //블럭을 둘 수 있는지 체크하기
            if (!Block_Put_Possible_Check())
            {
                //블럭 둘곳이 없으므로 유저 상태 변경
                idle = true;
            }
            else
            {
                idle = false;
            }

        }

        private bool Block_Put_Possible_Check()
        {
            //현재 리필된 블럭이 라인클리어 포함해서 둘수 있는지 체크
            //블럭 리필 순서대로 모든 로테이션에서 둘수있는지 체크
            //최상단에서부터 체크하고 하나라도 둘 수 있으면 바로 브레이크
            //제일 위에 15번째 줄보다는 14번째 줄부터 체크하는게 낫다
            //제일 왼쪽 첫번째 줄보다는 두번째 줄부터 체크하는게 낫다
            //빈 블록만 검사

            List<Pos> pos_list;
            int count = 0;


            for (short i = 13; i >= 0; i++)
            {
                for (short j = 1; j < 10; j++)
                {
                    if (m_brick_fill[j][i])
                        continue;
                    for (short k = 0; k < 3; k++)
                    {

                        for (short m = 0; m < Blocks[Refill_Blocks[k]].Get_Max_Rotation_Count(); m++)
                        {
                            pos_list = Blocks[Refill_Blocks[k]].Calc(j, i, m);
                            foreach (var child in pos_list)
                            {
                                if (!Block_Empty_Check(child.x, child.y))
                                {
                                    count++;
                                }
                                else
                                    break;
                            }
                            if (count == pos_list.Count)
                            {
                                //블럭을 놓을수 있다.
                                goto EXIT;

                            }
                            count = 0;
                        }
                    }
                }
            }

            //15번째줄 최상단 검사
            for (short i = 0; i < 10; i++)
            {
                if (m_brick_fill[i][14])
                    continue;
                for (short k = 0; k < 3; k++)
                {
                    
                    for (short m = 0; m < Blocks[Refill_Blocks[k]].Get_Max_Rotation_Count(); m++)
                    {
                        pos_list = Blocks[Refill_Blocks[k]].Calc(i, 14, m);
                        foreach (var child in pos_list)
                        {
                            if (!Block_Empty_Check(child.x, child.y))
                            {
                                count++;
                            }
                            else
                                break;
                        }
                        if (count == pos_list.Count)
                        {
                            //블럭을 놓을수 있다.
                            goto EXIT;

                        }
                        count = 0;
                    }
                }
            }

            //x=0 라인 검사
            for (short i = 0; i < 14; i++)
            {
                if (m_brick_fill[0][i])
                    continue;
                for (short k = 0; k < 3; k++)
                {
                    
                    for (short m = 0; m < Blocks[Refill_Blocks[k]].Get_Max_Rotation_Count(); m++)
                    {
                        pos_list = Blocks[Refill_Blocks[k]].Calc(0, i, m);
                        foreach (var child in pos_list)
                        {
                            if (!Block_Empty_Check(child.x, child.y))
                            {
                                count++;
                            }
                            else
                                break;
                        }
                        if (count == pos_list.Count)
                        {
                            //블럭을 놓을수 있다.
                            goto EXIT;

                        }
                        count = 0;
                    }
                }
            }

            return false;


            EXIT:
            return true;
        }

        public bool Line_Up(int count, int x)
        {

            //count 만큼 전체 블럭 올리기
            //두칸 이상 올리는 경우에는 동일한 x위치에 빈블럭
            //칸 올리는 정해진 랜덤시드 하나 더 필요하다.
            //전체 블럭 올리면서 게임 종료 체크 하기(꼭대기 블럭을 넘어가면 게임 종료)

            //브릭 올릴때 맨 위에서부터 올린다
            for(int i = field_height_count - 1; i >= 0; i--)
            {
                for(int j = 0; j < field_width_count; j++)
                {
                    //count만큼 올리는데 채워진 브릭이 꼭대기 브릭보다 높으면 게임 종료임
                    if(field_height_count - count - 1 < i)
                    {
                        if (m_brick_fill[j][i])
                        {
                            //count 만큼 올리게 되면 게임 게임 패배임
                            playing = false;
                            return false;
                        }
                        continue;
                    }
                    else
                    {
                        m_brick_fill[j][i + count] = m_brick_fill[j][i];
                    }
                }
            }
            //아래 블럭 변환 시키기 count아래 칸들 x위치는 빈블럭
            for(int i = 0; i < count; i++)
            {
                for(int j = 0; j < field_width_count; j++)
                {
                    if(j == x)
                    {
                        m_brick_fill[j][i] = false;
                        continue;
                    }
                    m_brick_fill[j][i] = true;
                }
            }
            //블럭을 둘 수 있는지 체크하기
            if (!Block_Put_Possible_Check())
            {
                //블럭 둘곳이 없으므로 유저 상태 변경
                idle = true;
            }
            else
            {
                idle = false;
            }
            return true;
        }

        public int get_attack_count()
        {
            return attack_count;
        }

        public int get_line_up_empty_x()
        {
            return line_up_empty_x;
        }

        public void game_reset()
        {
            //정보 초기화 
            for(int i = 0; i < field_width_count; i++)
            {
                for(int j = 0; j < field_height_count; j++)
                {
                    m_brick_fill[i][j] = false;
                }
            }
            LineClear_Y_List.Clear();
            empty_count = 0;
            combo_count = 0;
            line_clear_count = 0;
            attack_count = 0;
            brick_empty_count = 0;
            line_up_empty_x = 0;
            user_info.reset_info();
            idle = false;
        }
    }
}
