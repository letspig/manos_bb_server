﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManosNet;

namespace Manos_Block_Battle_GameServer
{
    class MatchService
    {
        ServerConnectService match_connect_servise;
        private int server_index;

        public MatchService()
        {
            match_connect_servise = new ServerConnectService();
            match_connect_servise.appcallback_on_message += on_message;
            match_connect_servise.appcallback_on_status_changed += on_status_changed;
        }

        public void Set_ServerIndex(int server_index)
        {
            this.server_index = server_index;
        }

        public void Match_Server_Connect(string address, int port)
        {
            match_connect_servise.connect_server(address, port);
        }

        void on_status_changed(NETWORK_EVENT status)
        {
            switch (status)
            {
                // 접속 성공.
                case NETWORK_EVENT.connected:
                    {
                        Console.WriteLine("match_server connected!");
                    }
                    break;

                // 연결 끊김.
                case NETWORK_EVENT.disconnected:

                    break;

            }
        }

        void on_message(Packet msg)
        {
            //this.message_receiver.SendMessage("on_recv", msg);
            PROTOCOL_GAMESERVER_MATCH id = (PROTOCOL_GAMESERVER_MATCH)msg.pop_protocol_id();
            switch (id)
            {
                case PROTOCOL_GAMESERVER_MATCH.CREATE_ROOM_REQUEST:
                    //매치서버 요청번호, 게임모드, 인원수 
                    //msg.pop_int16();
                    //room_create_call(msg);
                    
                    //Program.create_room(msg, server_index);
                    break;
            }
        }

        public void Send(Packet msg)
        {
            match_connect_servise.send(msg);
        }
    }
}
