﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manos_Block_Battle_GameServer
{
    class FieldLogicProcess
    {
        //최상단 y값, 현재 콤보수
        private short top_y;
        private bool playing;
        private short ranking;
        private short team_index;
        private short mission_line_clear_count_max = 20;
        private short line_clear_count = 0;
        private bool mission_state = true;
        public FieldLogicProcess()
        {
            Reset();
        }

        public void Set_Team_Index(short index)
        {
            team_index = index;
        }

        public short Get_Team_Index()
        {
            return team_index;
        }

        public void Set_Ranking(short ranking)
        {
            this.ranking = ranking;
        }

        public short Get_Ranking()
        {
            return ranking;
        }

        public bool Get_Playing()
        {
            return playing;
        }

        public bool Mission_Check(short clear_count)
        {
            //max line clear 가 되면 true return
            if (mission_state)
            {
                if(line_clear_count < mission_line_clear_count_max)
                {
                    line_clear_count += clear_count;
                }

                if(line_clear_count >= mission_line_clear_count_max)
                {
                    mission_state = false;
                    //여기서 미션이 완료되었고 미션 로직 처리한다.
                    return true;
                }
            }
            return false;
        }

        public void Change_Top_Y_Value(short y)
        {
            top_y += y;
            if (top_y < 0)
                top_y = 0;
            if (top_y > 14)
                top_y = 14;
        }

        public void Logic_Process(short y)
        {
            top_y = y;
        }

        public bool Game_End_Check()
        {
            if (top_y > 14)
            {
                playing = false;
                return true;
            }
            return false;
        }

        public void GameEndCheck()
        {
            //y값이 14보다 크면 게임 엔드
            if(top_y > 14)
            {
                playing = false;
            }
        }

        public void Increase_Top_Y(short count)
        {
            top_y += count;
            GameEndCheck();
        }

        public void Set_Playing(bool playing)
        {
            this.playing = playing;
        }

        public void Reset()
        {
            top_y = 0;
            playing = false;
            ranking = 4;
            team_index = 0;
            line_clear_count = 0;
            mission_state = true;
        }
    }
}
