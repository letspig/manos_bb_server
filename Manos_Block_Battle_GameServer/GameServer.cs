﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ManosNet;

namespace Manos_Block_Battle_GameServer
{
    class GameServer
    {
        //게임 룸 생성 및 삭제, 리스트 관리하기
        private Dictionary<int, GameRoom> rooms_dic;
        private RoomPool m_roomPool;
        private int max_room_count = 900;
        private object operation_lock;
        private Queue<Packet> user_operations;
        private Thread logic_Thread;
        private AutoResetEvent loop_event;
        public delegate void Create_Room_Complete_Callback(int match_index, short room_number, int server_index);
        public Create_Room_Complete_Callback room_create_complete;

        private short room_number;
        private Queue<short> room_number_queue;
        
        public GameServer()
        {
            room_number_queue = new Queue<short>(max_room_count);
            operation_lock = new object();
            loop_event = new AutoResetEvent(false);
            user_operations = new Queue<Packet>();

            m_roomPool = new RoomPool(max_room_count);
            rooms_dic = new Dictionary<int, GameRoom>();
            for (short i=0; i< max_room_count; i++)
            {
                GameRoom gameRoom = new GameRoom();
                m_roomPool.Push(gameRoom);
                room_number_queue.Enqueue((short)(i+100));
            }
            

            logic_Thread = new Thread(gameLoop);
            logic_Thread.Start();
        }

        private void gameLoop()
        {
            while (true)
            {
                Packet packet = null;
                lock (operation_lock)
                {
                    if(user_operations.Count > 0)
                    {
                        packet = user_operations.Dequeue();
                    }
                }

                if(packet != null)
                {
                    //packet 처리
                    process_receive(packet);
                }

                if(user_operations.Count <= 0)
                {
                    loop_event.WaitOne();
                }
            }
        }

        public void Enqueue_Packet(Packet packet)
        {
            lock (operation_lock)
            {
                user_operations.Enqueue(packet);
                loop_event.Set();
            }
        }

        private void process_receive(Packet packet)
        {
            packet.owner.process_user_operation(packet);
        }

        public void EnterGameRoom(Packet msg, GameUser user)
        {
            short room_number = msg.pop_int16();
            if (room_number < 100 || room_number > 999)
                return;
            
            lock (rooms_dic)
            {
                if (rooms_dic.ContainsKey(room_number))
                {
                    GameRoom existRoom = rooms_dic[room_number];
                    existRoom.Add_Normal_User(user);
                    user.SetGameRoom(existRoom);
                    existRoom = null;
                }
                else
                {
                    //최초 입장이거나 잘못된 방
                    GameRoom newRoom = m_roomPool.Pop();
                    newRoom.Normal_Init();
                    newRoom.Add_Normal_User(user);
                    user.SetGameRoom(newRoom);
                    rooms_dic.Add(room_number_queue.Dequeue(), newRoom);
                    newRoom = null;
                }
            }
            

        }

        public void CreateGameRoom(Packet msg, MatchService service)
        {
            //요청번호, 게임모드, 인원수 
            short request_num = msg.pop_int16();
            GAME_MODE mode = (GAME_MODE)msg.pop_int16();
            short num = msg.pop_int16();
            short room_number;
            lock (rooms_dic)
            {
                room_number = get_create_room_number();
                
                if (room_number == -1)
                    return;
                GameRoom gameRoom = m_roomPool.Pop();
                gameRoom.Init(mode, num);
                rooms_dic.Add(room_number, gameRoom);
                Packet create_complete_msg = Packet.create((short)PROTOCOL_GAMESERVER_MATCH.CREATE_ROOM_COMPLETE);
                create_complete_msg.push(request_num);
                create_complete_msg.push(room_number);
                service.Send(create_complete_msg);

            }
        }

        public void CreateGameRoom(Packet msg, int server_index)
        {
            //인원수, 유저 고유 id, 닉네임, 점수, 팀정보(solo, red, blue)
            GameRoom gameRoom = m_roomPool.Pop();
            
            lock (rooms_dic)
            {
                room_number = get_create_room_number();
                if (room_number == -1)
                    return;
                
                rooms_dic.Add(room_number, gameRoom);
            }
            int match_index = msg.pop_int32();
            gameRoom.Init(msg, room_number);
            //방생성 완료 콜백 매치서버로 보내기
            room_create_complete(match_index, room_number, server_index);



        }

        private short get_create_room_number()
        {
            //0부터 999까지 방번호 존재
            //낮은 번호부터 사용 0~999까지 쓰다가 0번이나 중간의 번호가 삭제 되면 중간 번호 사용 가능
            //큐에 넣어놓고 사용할때 빼왔다가 다쓰면 다시 큐에 넣고 ok?
            lock (room_number_queue)
            {
                if (room_number_queue.Count > 0)
                {
                    return room_number_queue.Dequeue();
                }
                return -1;
            }
            
        }

        public void Game_End(GameRoom room)
        {
            //사용한 룸 반납
            //룸 dic 에서 삭제
            lock (operation_lock)
            {
                m_roomPool.Push(room);
                room_number_queue.Enqueue(room.Get_Room_Number());
                remove_GameRoom(room.Get_Room_Number());
            }
        }

        public void remove_GameRoom(int roomNumber)
        {
            lock (rooms_dic)
            {
                if (rooms_dic.ContainsKey(roomNumber))
                    rooms_dic.Remove(roomNumber);
            }
        }
    }
}
