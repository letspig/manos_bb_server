﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manos_Block_Battle_GameServer
{
    class GameLogicProcess
    {
        //각 유저 필드 로직 프로세스 풀에서 하나씩 빼와서 사용
        private Stack<FieldLogicProcess> field_logic_process_stack;
        private FieldLogicProcess[] field_logic_array;

        private short max_field_count = 4;
        private short current_user_count;
        private short ranking_count;
        private List<short> death_user_index_list;
        private List<short> live_user_index_list;

        public delegate void Logic_Result_Callback(List<short> fied_index_list, short game_result_rank);
        public delegate void Lose_Player_Callback_Normal(short field_index, short rank);
        public Lose_Player_Callback_Normal Lose_Call_Normal;

        public delegate void Block_Put_Delegate(GameUser user, short field_index, short changed_y_value);
        public Block_Put_Delegate Block_put_del;

        public bool block_put_process(short field_index, short changed_y_value)
        {
            //그냥 내 필드 블럭 풋만 하는거여
            if (!field_logic_array[field_index].Get_Playing())
                return false;
            field_logic_array[field_index].Change_Top_Y_Value(changed_y_value);
            return true;
        }

        

        public void process(short field_index, short top_y, short attack_count, Logic_Result_Callback callback)
        {
            //필드 인덱스로 각 유저 로직 처리하고 결과 반영
            if (!field_logic_array[field_index].Get_Playing())
                return;

            field_logic_array[field_index].Logic_Process(top_y);

            foreach(var child in live_user_index_list)
            {
                if (field_index == child)
                    continue;
                if(field_logic_array[field_index].Get_Team_Index() == field_logic_array[child].Get_Team_Index())
                {
                    if (field_logic_array[field_index].Get_Team_Index() != 0)
                        continue;
                }
                if(attack_count > 0)
                    field_logic_array[child].Increase_Top_Y(attack_count);
            }

            //게임 결과 체크
            //동시 순위 반영함 

            foreach (var child in live_user_index_list)
            {
                if (!field_logic_array[child].Get_Playing())
                {
                    field_logic_array[child].Set_Ranking(ranking_count);
                    death_user_index_list.Add(child);
                    live_user_index_list.Remove(child);
                }
            }

            if(death_user_index_list.Count > 0)
            {
                //플레이어 누구누구 죽었는지 콜백
                callback(death_user_index_list, ranking_count);
                ranking_count -= (short)death_user_index_list.Count;
                death_user_index_list.Clear();
            }

            if(ranking_count == 1)
            {
                callback(live_user_index_list, ranking_count);
            }
        }

        public void GameStart()
        {
            for(short i = 0; i < current_user_count; i++)
            {
                field_logic_array[i].Set_Playing(true);
                live_user_index_list.Add(i);
            }
        }

        public void Add_User(short field_index)
        {
            //stack에서 하나씩 빼서 등록
            if (field_logic_process_stack.Count < 1)
                return;
            field_logic_array[field_index] = field_logic_process_stack.Pop();
            current_user_count++;
        }

        public void Reset()
        {
            //게임룸 사라질 때 리셋
            for(short i = 0; i < max_field_count; i++)
            {
                if (field_logic_array[i] == null)
                    continue;
                field_logic_array[i].Reset();
                field_logic_process_stack.Push(field_logic_array[i]);
            }
            current_user_count = 0;
            ranking_count = 4;
            death_user_index_list.Clear();
            live_user_index_list.Clear();
        }

        public GameLogicProcess()
        {
            current_user_count = 0;
            ranking_count = 4;
            death_user_index_list = new List<short>();
            live_user_index_list = new List<short>();
            field_logic_process_stack = new Stack<FieldLogicProcess>();
            field_logic_array = new FieldLogicProcess[max_field_count];
            for(short i = 0; i < max_field_count; i++)
            {
                FieldLogicProcess field_logic = new FieldLogicProcess();
                field_logic_process_stack.Push(field_logic);
                field_logic_array[i] = null;
            }
        }

    }
}
