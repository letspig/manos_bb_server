﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manos_Block_Battle_GameServer
{

    public enum GAME_MODE
    {
        NORMAL_GAME,
        RANK_GAME,
        END
    }

    public enum PROTOCOL_CLIENT_GAMESERVER : short
    {
        HEART_BEAT,
        ENTER_GAME_ROOM,
        BLOCK_INFO,
        GAME_RESULT,
        ALL_USER_ENTER,
        GAME_START,
        GAME_READY,
        END
    }

    public enum PROTOCOL_GAMESERVER_MATCH : short
    {
        CREATE_ROOM_REQUEST,
        CREATE_ROOM_COMPLETE,
        GAME_ROOM_DELETE,
        END
    }
}
