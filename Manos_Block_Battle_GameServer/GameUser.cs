﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManosNet;

namespace Manos_Block_Battle_GameServer
{
    class GameUser : IPeer
    {
        UserToken token;
        GameRoom gameRoom;
        User_Info user_info;
       
        public GameUser()
        {
            user_info = new User_Info();
        }
        public GameUser(UserToken token)
        {
            this.token = token;
            this.token.set_peer(this);
        }

        public void Set_UserToken(UserToken token)
        {
            this.token = token;
            this.token.set_peer(this);
        }


        public void Set_User_Ready(short ready)
        {
            user_info.ready = ready;
        }

        public void Set_User_Field_Index(short index)
        {
            user_info.field_index = index;
        }

        public void Set_User_Info(User_Info user_info)
        {
            this.user_info = user_info;
        }

        public User_Info Get_User_Info()
        {
            return user_info;
        }

        public void SetGameRoom(GameRoom gameRoom)
        {
            this.gameRoom = gameRoom;
        }

        void IPeer.on_message(byte[] buffer)
        {
            // ex)
            byte[] clone = new byte[1024];
            Array.Copy(buffer, clone, buffer.Length);
            Packet msg = new Packet(clone, this);
            Program.gameServer.Enqueue_Packet(msg);
            
        }

        void IPeer.process_user_operation(Packet msg)
        {
            PROTOCOL_CLIENT_GAMESERVER protocol = (PROTOCOL_CLIENT_GAMESERVER)msg.pop_protocol_id();
            switch (protocol)
            {


                case PROTOCOL_CLIENT_GAMESERVER.BLOCK_INFO:
                    //어떤 블럭(Block_type)을 어느 위치(x,y)에 어떤 방향(rotation)의 정보가 담겨있음
                    if (gameRoom == null)
                        return;
                    gameRoom.Block(user_info.field_index, msg);
                    break;

                case PROTOCOL_CLIENT_GAMESERVER.ENTER_GAME_ROOM:
                    
                    //게임 룸 번호 유저 정보
                    //nickname, teamindex
                    user_info.nick_name = msg.pop_string();
                    short team_index = msg.pop_int16();
                    if (team_index < 0 || team_index > 2)
                        return;
                    Program.gameServer.EnterGameRoom(msg, this);
                    break;

                case PROTOCOL_CLIENT_GAMESERVER.GAME_READY:
                    //유저가 게임씬에 입장했다
                    if (gameRoom == null)
                        return;
                    gameRoom.User_Ready(this);
                    break;
            }
        }

        void IPeer.on_removed()
        {
            Console.WriteLine("The client disconnected.");
            if(gameRoom != null)
            {
                gameRoom.remove_User(this);
            }
            Program.remove_user(this);
        }

        public void send(Packet msg)
        {
            this.token.send(msg);
        }

        void IPeer.disconnect()
        {
            this.token.Get_Socket().Disconnect(false);
        }

        public UserToken Get_UserToken()
        {
            return token;
        }

    }
}
