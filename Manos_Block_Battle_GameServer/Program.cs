﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ManosNet;

namespace Manos_Block_Battle_GameServer
{
    class Program
    {
        static List<GameUser> userlist;
        public static GameUserPool user_pool;
        public static GameServer gameServer = new GameServer();
        private static object operation_lock = new object();
        static NetworkService networkService = new NetworkService();
        static MatchServerMgr match_server_mgr = new MatchServerMgr();
        private static int user_max_count = 2000;
        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.ProcessExit += CurrentDomain_ProcessExit;

            PacketBufferManager.initialize(8000);
            userlist = new List<GameUser>();
            user_pool = new GameUserPool(user_max_count);
            for(int i = 0; i < user_max_count; i++)
            {
                GameUser gameUser = new GameUser();
                user_pool.Push(gameUser);
            }
            gameServer.room_create_complete += create_room_complete;


            networkService.session_created_callback += on_session_created;
            networkService.initialize();
            networkService.listen("0.0.0.0", 7979, 1024);

            //match_server_mgr.Init();

            Console.WriteLine("Server Start");

            while (true)
            {
                System.Threading.Thread.Sleep(1000);
            }
        }

        static void CurrentDomain_ProcessExit(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }


        static void on_session_created(UserToken token)
        {
            lock (userlist)
            {
                GameUser user = user_pool.Pop();
                user.Set_UserToken(token);
                userlist.Add(user);
            }


        }



        public static void create_room(Packet msg)
        {
            //gameServer.CreateGameRoom(msg);
        }

        public static void create_room_complete(int match_index, short room_number, int server_index)
        {
            //매치 서버로 룸 생성 번호 보내주기
            Packet msg = Packet.create((short)PROTOCOL_GAMESERVER_MATCH.CREATE_ROOM_COMPLETE);
            msg.push(match_index);
            msg.push(room_number);
            //match_server_mgr.Send(server_index, msg);
        }

        public static void remove_user(GameUser user)
        {
            lock (userlist)
            {
                user_pool.Push(user);
                userlist.Remove(user);
            }
        }

        public static void Enter_Game_Room(int room_number, GameUser gameUser)
        {
            //lock(gameServer)
        }

        public static void returnEventArgs(UserToken token)
        {
            lock (operation_lock)
            {
                networkService.close_clientsocket(token);
            }
        }
    }
}
