﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manos_Block_Battle_GameServer
{
    class GameLogic
    {
        //4개의 게임 필드 미리 생성
        private Game_Field[] game_field;
        private int max_game_field_count = 4;
        private short current_user_count = 0;
        private short game_result_count;
        private List<short> death_user_index_list;
        private List<short> live_user_index_list;

        public delegate void Logic_Result_Callback(List<short> fied_index_list, short game_result_rank);

        public GameLogic()
        {
            game_field = new Game_Field[max_game_field_count];

            for (int i = 0; i < max_game_field_count; i++)
            {
                game_field[i] = new Game_Field();
            }
            death_user_index_list = new List<short>();
            live_user_index_list = new List<short>();
        }

        public void Set_RandomSeed(uint seed)
        {
            for (int i = 0; i < max_game_field_count; i++)
            {
                game_field[i].init(seed);
            }
        }
        public void Init()
        {
            
        }

        public void Add_Normal_User_Info(GameUser user)
        {
            game_field[current_user_count].Set_User_Info(user.Get_User_Info().team_index);
            user.Set_User_Field_Index(current_user_count);
            current_user_count++;
            game_result_count++;
        }

        public void Add_user_info(string user_id, string nickname, short rank_score, short team_index)
        {
            game_field[current_user_count].Set_User_Info(user_id, nickname, rank_score, team_index);
            current_user_count++;
            game_result_count++;
        }



        public User_Info Find_User_Info(string user_id)
        {
            for(int i = 0; i < current_user_count; i++)
            {
                if(game_field[i].Get_User_Info().user_id == user_id)
                {
                    return game_field[i].Get_User_Info();
                }
            }
            return null;
        }


        public void Logic(User_Info user_info, int refill_num, short x, short y, short rotation, Logic_Result_Callback callback)
        {
            if (game_result_count == 1)
                return;
            game_field[user_info.field_index].Block_Field_On(refill_num, x, y, rotation);
            if (!game_field[user_info.field_index].playing)
            {
                //내가 놓은 블럭에 내가 죽음
                game_field[user_info.field_index].game_result = game_result_count;
                death_user_index_list.Add((short)user_info.field_index);

            }
            //콤보 또는 3줄이상 라인클리어로 공격을 했는지 검사해서 적팀 gamefield line 올린다.
            //자신을 제외하고 팀 인덱스가 다른 인덱스 유저 모두에게 공격
            //3인 이상의 게임에서 한 유저가 게임이 끝나면 그 유저 로직은 돌지 않는다.
            int attack_count = game_field[user_info.field_index].get_attack_count();
            if (attack_count > 0)
            {
                int line_up_empty_x = game_field[user_info.field_index].get_line_up_empty_x();

                foreach (var child in live_user_index_list)
                {
                    if (child == user_info.field_index)
                        continue;
                    if (user_info.team_index == game_field[child].Get_User_Info().team_index)
                    {
                        if (user_info.team_index != 0)
                            continue;
                    }

                    if (!game_field[child].playing)
                        continue;

                    if (!game_field[child].Line_Up(attack_count, line_up_empty_x))
                    {
                        game_field[child].playing = false;
                        game_field[child].game_result = game_result_count;
                        death_user_index_list.Add((short)child);
                    }
                }
            }
            

            if(death_user_index_list.Count > 0)
            {
                foreach (var child in death_user_index_list)
                {
                    live_user_index_list.Remove(child);
                }

                callback(death_user_index_list, game_result_count);
                game_result_count -= (short)death_user_index_list.Count;
                death_user_index_list.Clear();
            }

            if(game_result_count == 1)
            {
                //게임 완전 종료 1등 뽑음
                //팀게임이면 팀원 승리 해줌(대신에 그냥 1등보다는 점수 조금 오름)
                callback(live_user_index_list, game_result_count);
                live_user_index_list.Clear();

            }

            int idle_count = 0;
            foreach(var child in live_user_index_list)
            {
                if (game_field[child].idle)
                {
                    //현재 생존한 유저 모두가 idle이면 무승부 처리
                    idle_count++;
                }
            }
            if(idle_count == live_user_index_list.Count)
            {
                //무승부 처리하기 callback
                callback(live_user_index_list, 0);
            }

            //이번 로직에서 두명이상이 게임패배라면 공둥 순위다 
            //패배 목록 카운터가 0 이상이면 룸에 게임 엔드 콜백 
            
        }

        public short Get_Used_Block_Type(short field_index)
        {
            return game_field[field_index].used_block_type;
        }

        public void GameStart()
        {
            for(short i = 0; i < current_user_count; i++)
            {
                game_field[i].playing = true;
                live_user_index_list.Add(i);
            }
        }

        public void reset()
        {
            current_user_count = 0;
            death_user_index_list.Clear();
            live_user_index_list.Clear();

            for (short i = 0; i < max_game_field_count; i++)
            {
                game_field[i].game_reset();
            }
            
        }
    }
}
