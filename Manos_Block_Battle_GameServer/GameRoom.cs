﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using ManosNet;

namespace Manos_Block_Battle_GameServer
{
    class GameRoom
    {
        //팀전, 개인전에 따라서 init 다르게
        private short roomNumber;
        private List<GameUser> user_List;
        private int max_user_count;
        private short current_user_count;
        //private GameLogic game_logic;
        private GameLogicProcess game_logic_process;
        private Well_Random w_random;
        private Timer pTimer;
        private short ready_count;
        private short room_state;
        private Stack<short> field_index_stack;

        public delegate void Block_Put_Delegate(short field_index, Packet msg);

        public GameRoom()
        {
            user_List = new List<GameUser>();
            field_index_stack = new Stack<short>();
            for(short i = 3; i >= 0; i--)
            {
                field_index_stack.Push(i);
            }
            //game_logic = new GameLogic();
            game_logic_process = new GameLogicProcess();
            w_random = new Well_Random();
            Random random = new Random();
            w_random.Set_Seed((uint)random.Next(10, 20000));
            //pTimer = new Timer(10000);
            //pTimer.Elapsed += OnTimedEvent;
            //pTimer.AutoReset = true;
            //pTimer.Enabled = false;

        }

        public void Reset()
        {
            max_user_count = 2;
            current_user_count = 0;
            ready_count = 0;
            room_state = 0;
        }

        private void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            //일정 시간마다 heartbeat 체크
            //빈방이나 게임을 하지 않는 유저 또는 비정상적으로 종료된 유저 삭제하기 위함
            foreach(var child in user_List)
            {
                Packet heartbeat = Packet.create((short)PROTOCOL_CLIENT_GAMESERVER.HEART_BEAT);
                child.send(heartbeat);
            }

            //pTimer.Stop();
        }

        public void remove_User(GameUser user)
        {
            lock (user_List)
            {
                if (user_List.Contains(user))
                {
                    user_List.Remove(user);
                }
            }
            
        }
        public short Get_Room_Number()
        {
            return roomNumber;
        }

        public void Init(GAME_MODE mode)
        {
            //room reset 하기 
            //mode에 맞는 콜백함수 셋
            Reset();
            switch (mode)
            {
                case GAME_MODE.NORMAL_GAME:

                    break;

                case GAME_MODE.RANK_GAME:

                    break;
            }
        }

        public void Init(GAME_MODE mode, short num)
        {
            //게임모드에 따라서 콜백함수 셋팅하기 
            //최대 인원수 셋 
            max_user_count = num;
            Reset();
            game_logic_process.Reset();
        }

        public void Normal_Init()
        {
            //일반게임 init
            //게임 시작 종료등 랭겜과 다른처리 되도록 콜백함수 따로 셋
            Reset();
            //game_logic.reset();
            game_logic_process.Reset();
        }


        public void Init(Packet packet, short room_number)
        {
            //매치서버로부터 게임 셋팅 요청 받음
            //패킷 순서대로 게임 필드 셋팅
            roomNumber = room_number;
            string user_id, nick_name;
            short rank_score, team_index;

            //game_logic.reset();

            this.max_user_count = packet.pop_int16();
            for(int i = 0; i < max_user_count; i++)
            {
                //인원수, 유저 고유 id, 닉네임, 점수, 팀정보(solo, red, blue) x max_user_count
                user_id = packet.pop_string();// 고유 id
                nick_name = packet.pop_string();// 닉네임
                rank_score = packet.pop_int16();// 점수
                team_index = packet.pop_int16();// 팀정보

                //game_logic.Add_user_info(user_id, nick_name, rank_score, team_index);
            }
            current_user_count = 0;
            pTimer.Enabled = true;

            //room set 완료 되면 매치서버로 완료 되었다고 알려줘야 한다
            //매치서버는 완료 메세지를 받으면 매칭된 클라이언트한테 룸에 입장하라고 알려준다.
            //Program.create_room_complete(roomNumber);
        }

        public void User_Ready(GameUser user)
        {
            if (user_List.Count == ready_count)
                return;
            if (user.Get_User_Info().ready == 1)
                return;
            user.Set_User_Ready(1);
            ready_count++;
            if (user_List.Count == ready_count)
            {
                foreach(var child in user_List)
                {
                    Packet game_start_msg = Packet.create((short)PROTOCOL_CLIENT_GAMESERVER.GAME_START);
                    child.send(game_start_msg);
                }
            }
        }

        public void Add_User(GameUser user)
        {
            //로직 프로세스에 유저 등록 및 콜백 등록
            if (max_user_count == current_user_count)
                return;
            if (user_List.Contains(user))
                return;
            user.Set_User_Field_Index(field_index_stack.Pop());
            user_List.Add(user);

            game_logic_process.Add_User(user.Get_User_Info().field_index);
            current_user_count++;

            Packet msg = Packet.create((short)PROTOCOL_CLIENT_GAMESERVER.ENTER_GAME_ROOM);
            msg.push(user.Get_User_Info().field_index);
            user.send(msg);

            if (max_user_count == current_user_count)
            {
                //모든 유저가 룸에 입장 했다.
            }


        }

        public void Add_Normal_User(GameUser user)
        {
            if (max_user_count == current_user_count)
                return;

            if (!user_List.Contains(user))
            {

                user.Set_User_Field_Index(field_index_stack.Pop());
                user_List.Add(user);
                //game_logic.Add_Normal_User_Info(user);
                
                game_logic_process.Add_User(user.Get_User_Info().field_index);
                current_user_count++;

                Packet msg = Packet.create((short)PROTOCOL_CLIENT_GAMESERVER.ENTER_GAME_ROOM);
                msg.push(user.Get_User_Info().field_index);
                user.send(msg);
            }

            if (max_user_count == current_user_count)
            {
                NormalGameStart();
            }
        }

        public void AddUser(GameUser user)
        {
            //입장하면 자신의 정보(고유 id)로 자신의 게임 필드 찾고
            //해당 필드 인덱스와 팀정보 받아와서 셋
            
            if (max_user_count == current_user_count)
                return;

            //user_id

            if (!user_List.Contains(user))
            {
                user_List.Add(user);
                //user.Set_User_Info(game_logic.Find_User_Info(user.Get_User_Info().user_id));
                current_user_count++;
            }

            if (max_user_count == current_user_count)
            {
                GameStart();
            }

        }

        private void NormalGameStart()
        {
            uint seed = w_random.Next(10, 20000);
            //game_logic.Set_RandomSeed(seed);

            foreach (var child in user_List)
            {
                Packet msg = Packet.create((short)PROTOCOL_CLIENT_GAMESERVER.ALL_USER_ENTER);
                msg.push((short)seed);
                msg.push((short)1);//mission
                msg.push(current_user_count);
                foreach (var child2 in user_List)
                {
                    msg.push(child2.Get_User_Info().field_index);
                    //msg.push(child2.Get_User_Info().nick_name);
                }
                child.send(msg);
            }

            //game_logic.GameStart();
            game_logic_process.GameStart();
        }

        private void GameStart()
        {
            //정해진 랜덤 시드 정해서 셋 하고 
            //모든 클라한테 게임 시작 패킷 보내기(씨드번호, 유저수, 각 유저 정보(필드 인덱스, 팀인덱스, 닉네임, 랭크점수)
            uint seed = w_random.Next(10, 20000);
            //game_logic.Set_RandomSeed(seed);

            foreach (var child in user_List)
            {
                Packet msg = Packet.create((short)PROTOCOL_CLIENT_GAMESERVER.GAME_START);
                msg.push((short)seed);
                msg.push(current_user_count);
                foreach (var child2 in user_List)
                {
                    msg.push(child2.Get_User_Info().field_index);
                    msg.push(child2.Get_User_Info().team_index);
                    msg.push(child2.Get_User_Info().nick_name);
                    msg.push(child2.Get_User_Info().rank_score);
                }
                child.send(msg);
            }

            //game_logic.GameStart();
        }

        private void GameEnd()
        {
            //게임 완전 종료시 호출
            //3인 이상 게임에서 한명이 종료되도 호출하지 않을것.
            pTimer.Stop();
            current_user_count = 0;
            Program.gameServer.Game_End(this);
            
        }

        public void Block(short field_index, Packet msg)
        {
            short refill_num = msg.pop_int16();
            short dir = msg.pop_int16();
            short x = msg.pop_int16();
            short changed_y_value = msg.pop_int16();
            short attack_count = msg.pop_int16();
            game_logic_process.process(field_index, changed_y_value, attack_count, Logic_Result);

            foreach(var child in user_List)
            {
                //if (child.Get_User_Info().field_index == field_index)
                    //continue;
                Packet block_msg = Packet.create((short)PROTOCOL_CLIENT_GAMESERVER.BLOCK_INFO);
                block_msg.push(field_index);
                block_msg.push(attack_count);
                block_msg.push(refill_num);
                block_msg.push(dir);
                block_msg.push(x);
                
                
                child.send(block_msg);
            }
        }

        public void Block_Put(GameUser user, Packet msg, short field_index)
        {
            //gamelogicprocess에 내 블럭 놓기
            //미리 로직프로세스에 유저 등록 및 콜백을 등록 해 놓자.(게임 패배 콜백)
            //끝났으면 결과 모든유저(자신포함)에게 보내기
            //랭크게임이면 db 반영하기
            short block_num = msg.pop_int16();
            short dir = msg.pop_int16();
            short x = msg.pop_int16();
            short changed_y_value = msg.pop_int16();

            game_logic_process.block_put_process(user.Get_User_Info().field_index, changed_y_value);

            
        }

        private void Logic_Result(List<short> list, short rank_result)
        {
            //field index, result 담은 게임 로직 결과 모든 클라한테 보내기
            if(rank_result > 1)
            {
                //개인전일 경우 순위대로 db에 점수 바로바로 적용.
                foreach (var child in user_List)
                {
                    foreach (var field_index in list)
                    {
                        Packet packet = Packet.create((short)PROTOCOL_CLIENT_GAMESERVER.GAME_RESULT);
                        packet.push(field_index);
                        packet.push(rank_result);
                        child.send(packet);
                    }
                }
            }
            else if(rank_result == 1)
            {
                //1등 뽑음
                //이미 2등이 나왔으므로 1등이 누구인지 클라들에게 알려줄 필요는 없다.
                //여기서는 팀전일 경우 같은 팀원 랭크 처리 해주면 된다.(같은 팀원이 접속해 있을 경우)
                //팀전일 경우에는 바로 db에 랭크 적용하지 않고 1등이 뽑혀야 적용한다.
                //일반 모드에서는 랭크 적용 안함.
                //게임 모드에 따라서 상속으로 게임룸 구현 해야 겠네 랭크 적용모드와 일반 모드, 아이템모드와 노템모드
            }
            else
            {
                //무승부 처리
                foreach (var child in user_List)
                {
                    foreach (var field_index in list)
                    {
                        Packet packet = Packet.create((short)PROTOCOL_CLIENT_GAMESERVER.GAME_RESULT);
                        packet.push(field_index);
                        packet.push(rank_result);
                        child.send(packet);
                    }
                }
            }
            
            
        }


    }
}
