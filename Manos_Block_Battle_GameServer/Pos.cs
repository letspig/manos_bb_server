﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manos_Block_Battle_GameServer
{
    class Pos
    {
        public int x, y;
        public Pos(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public void setPos(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }
}
