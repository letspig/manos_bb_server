﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManosNet;
using System.Threading;
using System.Net;

namespace Manos_Block_Battle_GameServer
{
    class ServerConnectService
    {
        ManosNetEventManager event_manager;
        Connector connector;
        IPeer gameserver;
        NetworkService service;

        private Thread logic_Thread;
        private AutoResetEvent loop_event;


        // 접속 완료시 호출되는 델리게이트. 어플리케이션에서 콜백 매소드를 설정하여 사용한다.
        public delegate void StatusChangedHandler(NETWORK_EVENT status);
        public StatusChangedHandler appcallback_on_status_changed;

        // 네트워크 메시지 수신시 호출되는 델리게이트. 어플리케이션에서 콜백 매소드를 설정하여 사용한다.
        public delegate void MessageHandler(Packet msg);
        public MessageHandler appcallback_on_message;

        public ServerConnectService()
        {
            this.event_manager = new ManosNetEventManager();
            loop_event = new AutoResetEvent(false);
            logic_Thread = new Thread(gameLoop);
            logic_Thread.Start();

        }

        public void connect_server(string host, int port)
        {
            this.service = new NetworkService();

            // endpoint정보를 갖고있는 Connector생성. 만들어둔 NetworkService객체를 넣어준다.
            connector = new Connector(service);
            // 접속 성공시 호출될 콜백 매소드 지정.
            connector.connected_callback += on_connected_matchserver;
            IPEndPoint endpoint = new IPEndPoint(IPAddress.Parse(host), port);
            connector.connect(endpoint);
        }

        private void on_connected_matchserver(UserToken token)
        {
            this.gameserver = new RemoteServerPeer(token);
            ((RemoteServerPeer)this.gameserver).set_eventmanager(this.event_manager);
            this.event_manager.enqueue_network_event(NETWORK_EVENT.connected);
        }

        public void send(Packet msg)
        {
            try
            {
                this.gameserver.send(msg);
                Packet.destroy(msg);
            }
            catch (Exception e)
            {
                //Debug.LogError(e.Message);
            }
        }

        private void gameLoop()
        {
            while (true)
            {
                // 수신된 메시지에 대한 콜백.
                if (this.event_manager.has_message())
                {
                    Packet msg = this.event_manager.dequeue_network_message();
                    if (this.appcallback_on_message != null)
                    {
                        this.appcallback_on_message(msg);
                    }
                }

                // 네트워크 발생 이벤트에 대한 콜백.
                if (this.event_manager.has_event())
                {
                    NETWORK_EVENT status = this.event_manager.dequeue_network_event();
                    if (this.appcallback_on_status_changed != null)
                    {
                        this.appcallback_on_status_changed(status);
                    }
                }
                else
                {
                    loop_event.WaitOne();
                }
            }
        }
    }
}
