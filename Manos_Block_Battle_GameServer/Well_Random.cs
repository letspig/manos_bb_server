﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manos_Block_Battle_GameServer
{
    class Well_Random
    {
        uint[] state = new uint[16];
        uint index = 0;

        public Well_Random()
        {

        }

        public void Set_Seed(uint seed)
        {
            for (int i = 0; i < 16; i++)
            {
                state[i] = seed;
                seed += seed;
            }
        }

        public uint Next(int minValue, int maxValue)
        {
            return (uint)((Next() % (maxValue - minValue)) + minValue);
        }

        public uint Next(uint maxValue)
        {
            return Next() % maxValue;
        }

        public uint Next()
        {
            uint a, b, c, d;

            a = state[index];
            c = state[(index + 13) & 15];
            b = a ^ c ^ (a << 16) ^ (c << 15);
            c = state[(index + 9) & 15];
            c ^= (c >> 11);
            a = state[index] = b ^ c;
            d = a ^ ((a << 5) & 0xda442d24U);
            index = (index + 15) & 15;
            a = state[index];
            state[index] = a ^ b ^ d ^ (a << 2) ^ (b << 18) ^ (c << 28);

            return state[index];
        }
    }
}
