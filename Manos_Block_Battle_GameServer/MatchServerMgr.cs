﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManosNet;

namespace Manos_Block_Battle_GameServer
{
    class MatchServerMgr
    {
        private MatchService match_service;
        private int max_match_service_count = 3;

        public MatchServerMgr()
        {
            match_service = new MatchService();
        }

        public void Init()
        {
            match_service.Match_Server_Connect("1234", 8989);
        }

        public void Send(int server_index, Packet msg)
        {
            match_service.Send(msg);
        }

    }
}
