﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manos_Block_Battle_GameServer
{
    class GameUserPool
    {
        Stack<GameUser> m_pool;

        public GameUserPool(int max_user)
        {
            m_pool = new Stack<GameUser>(max_user);
        }

        public void Push(GameUser user)
        {
            lock (m_pool)
            {
                m_pool.Push(user);
            }
        }

        public GameUser Pop()
        {
            lock (m_pool)
            {
                return m_pool.Pop();
            }
        }
    }
}
